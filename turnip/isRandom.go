package turnip

import "math"

// IsRandom determines if a week's prices could be random
func IsRandom(prices []float64) bool {
	phases := [6]int{0, 0, 0, 0, 0, -1}
	currentPhase := 0
	purchase := prices[0]

	minRate := 0.6
	maxRate := 0.8
	for i, price := range prices {
		if i == 0 {
			// Invalid purchase range
			if purchase < 90 || purchase > 110 {
				return false
			}
			continue
		}

		if price >= math.Ceil(0.9*purchase) &&
			price <= math.Ceil(1.4*purchase) {
			if currentPhase%2 == 1 {
				currentPhase += 1
			}
			phases[currentPhase] += 1
			minRate = 0.6
			maxRate = 0.8
		} else if price >= math.Ceil(minRate*purchase) &&
			price <= math.Ceil(maxRate*purchase) {
			minRate = (price-1)/purchase - 0.10
			maxRate = price/purchase - 0.04
			if currentPhase%2 == 0 {
				currentPhase += 1
			}
			phases[currentPhase] += 1
		} else {
			return false
		}

		if currentPhase > 4 || phases[0]+phases[2]+phases[4] > 7 ||
			phases[0] > 6 || phases[2] > 6 || phases[4] > 6 ||
			phases[1] > 3 || phases[3] > 3 || phases[1]+phases[3] > 5 {
			return false
		}
	}

	return true
}
