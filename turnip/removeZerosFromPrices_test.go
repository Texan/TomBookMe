package turnip_test

import (
	"testing"

	"gitlab.com/Texan/TomBookMe/turnip"
)

// Test that RemoveZerosFromPrices only removes trailing zeros
func Test_RemoveZerosShouldRemoveTrailingZeros(t *testing.T) {
	tests := [][]float64{
		{100, 88, 84, 80, 76, 72, 68, 64, 60, 0, 0, 0},
		{103, 93, 88, 84, 80, 76, 0, 0, 64, 0, 0, 0},
		{100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	}

	expected := [][]float64{
		{100, 88, 84, 80, 76, 72, 68, 64, 60},
		{103, 93, 88, 84, 80, 76, 0, 0, 64},
		{100},
		{},
	}

	for i, prices := range tests {
		result := turnip.RemoveZerosFromPrices(prices)
		if len(result) != len(expected[i]) {
			t.Errorf("Test case '%v' failed, expected: '%v', got: '%v'", prices, expected[i], result)
		}
		for k, v := range result {
			if v != expected[i][k] {
				t.Errorf("Test case '%v' failed, expected: '%v', got: '%v'", prices, expected[i], result)
			}
		}
	}
}
