package turnip

import "math"

func BestGuess(priceData []float64) []float64 {
	bestGuess := make([]float64, len(priceData))
	copy(bestGuess, priceData)
	if len(bestGuess) <= 1 {
		return bestGuess
	}

	// Determine purchase price
	for i, value := range bestGuess {
		if i == 0 && value == 0 {
			if bestGuess[1] == 0 {
				if bestGuess[2] == 0 {
					if bestGuess[3] != 0 {
						bestGuess[2] = bestGuess[3] + 4
						bestGuess[1] = bestGuess[2] + 4
						bestGuess[0] = math.Ceil(bestGuess[1] * 1.14)
						if bestGuess[0] < 90 || bestGuess[0] > 110 {
							bestGuess[0] = math.Ceil(bestGuess[1] * 0.87)
							if bestGuess[0] < 90 || bestGuess[0] > 110 {
								bestGuess[0] = 100
							}
						}
						i = 3
					} else {
						bestGuess[0] = 100
					}

				} else {
					bestGuess[1] = bestGuess[2] + 4
					bestGuess[0] = math.Ceil(bestGuess[1] * 1.14)
					if bestGuess[0] < 90 || bestGuess[0] > 110 {
						bestGuess[0] = math.Ceil(bestGuess[1] * 0.87)
						if bestGuess[0] < 90 || bestGuess[0] > 110 {
							bestGuess[0] = 100
						}
					}
					i = 2
				}
			} else {
				bestGuess[0] = math.Ceil(bestGuess[1] * 1.14)
				if bestGuess[0] < 90 || bestGuess[0] > 110 {
					bestGuess[0] = math.Ceil(bestGuess[1] * 0.87)
					if bestGuess[0] < 90 || bestGuess[0] > 110 {
						bestGuess[0] = 100
					}
				}
				i = 1
			}
			continue
		}

		if i == 1 && value == 0 {
			if bestGuess[2] != 0 {
				bestGuess[1] = bestGuess[2] + 4
			} else {
				bestGuess[1] = math.Ceil(bestGuess[0] * 0.875)
			}
			continue
		}

		if value == 0 {
			if bestGuess[i+1] != 0 {
				if bestGuess[i-1] > bestGuess[i+1] {
					bestGuess[i] = math.Ceil((bestGuess[i-1] + bestGuess[i+1]) / 2)
				} else {
					if bestGuess[i+1] > bestGuess[0]*0.9 && bestGuess[i+1] < bestGuess[0]*1.4 {
						if len(bestGuess)-2 == i {
							bestGuess[i] = bestGuess[i-1] - 4
						} else {
							if bestGuess[i+2] > bestGuess[0]*0.9 && bestGuess[i+2] < bestGuess[0]*1.4 {
								bestGuess[i] = bestGuess[i-1] - 4
							} else {
								bestGuess[i] = bestGuess[i+1] - 1
							}
						}
					}
				}
			} else {
				bestGuess[i] = bestGuess[i-1] - 4
			}
		}
	}

	return bestGuess
}
