package turnip

import (
	"math"
	"strconv"

	"gitlab.com/Texan/TomBookMe/database"
)

type Odds struct {
	LargeSpike   float64
	SmallSpike   float64
	Random       float64
	Decreasing   float64
	AveragePrice uint16
	MaxPrice     uint16
	MinPrice     uint16
	Unknown      bool
}

func (odds Odds) LargeSpikeRanking() int8 {
	ranking := int8(3)
	if odds.LargeSpike >= odds.SmallSpike {
		ranking -= 1
	}
	if odds.LargeSpike >= odds.Random {
		ranking -= 1
	}
	if odds.LargeSpike >= odds.Decreasing {
		ranking -= 1
	}
	return ranking
}

func (odds Odds) SmallSpikeRanking() int8 {
	ranking := int8(3)
	if odds.SmallSpike > odds.LargeSpike {
		ranking -= 1
	}
	if odds.SmallSpike >= odds.Random {
		ranking -= 1
	}
	if odds.SmallSpike >= odds.Decreasing {
		ranking -= 1
	}
	return ranking
}

func (odds Odds) RandomRanking() int8 {
	ranking := int8(3)
	if odds.Random > odds.LargeSpike {
		ranking -= 1
	}
	if odds.Random > odds.SmallSpike {
		ranking -= 1
	}
	if odds.Random >= odds.Decreasing {
		ranking -= 1
	}
	return ranking
}

func (odds Odds) DecreasingRanking() int8 {
	ranking := int8(3)
	if odds.Decreasing > odds.LargeSpike {
		ranking -= 1
	}
	if odds.Decreasing > odds.SmallSpike {
		ranking -= 1
	}
	if odds.Decreasing > odds.Random {
		ranking -= 1
	}
	return ranking
}

func CalculateOdds(priceData *database.Prices) Odds {
	pricesFloat := []float64{float64(priceData.Purchase), float64(priceData.MonAM), float64(priceData.MonPM), float64(priceData.TueAM), float64(priceData.TuePM), float64(priceData.WedAM), float64(priceData.WedPM), float64(priceData.ThuAM), float64(priceData.ThuPM), float64(priceData.FriAM), float64(priceData.FriPM), float64(priceData.SatAM), float64(priceData.SatPM)}

	pricesFloat = RemoveZerosFromPrices(pricesFloat)
	prices := BestGuess(pricesFloat)
	odds := DefaultOdds(database.GetUser(priceData.User))

	// Default case if we don't have a purchase price yet
	if len(pricesFloat) <= 1 {
		return odds
	}

	currentTime := len(pricesFloat) - 1

	isLargeSpike := IsLargeSpike(prices)
	isSmallSpike := IsSmallSpike(prices)
	isRandom := IsRandom(prices)
	isDecreasing := IsDecreasing(prices)

	remainingPercent := 1.00
	odds.MaxPrice = 0
	odds.MinPrice = 1000
	if !isLargeSpike {
		remainingPercent -= odds.LargeSpike
		odds.LargeSpike = 0
	} else if currentTime <= 7 {
		largeSpikeGone := odds.LargeSpike * (1.00 - ((8.00 - float64(currentTime)) / 7.00))
		remainingPercent -= largeSpikeGone
		odds.LargeSpike -= largeSpikeGone
		if uint16(prices[0])*6 > odds.MaxPrice {
			odds.MaxPrice = uint16(prices[0]) * 6
		}
		if uint16(prices[0])*2 < odds.MinPrice {
			odds.MinPrice = uint16(prices[0]) * 2
		}
	}

	if !isSmallSpike {
		remainingPercent -= odds.SmallSpike
		odds.SmallSpike = 0
	} else if currentTime <= 7 {
		if isLargeSpike && !isRandom {
			remainingPercent -= odds.SmallSpike * 0.9
			odds.SmallSpike *= 0.1
		} else if isRandom && !isLargeSpike {
			remainingPercent -= odds.SmallSpike * 0.6
			odds.SmallSpike *= 0.4
		}

		smallSpikeGone := odds.SmallSpike * (1.00 - ((8.00 - float64(currentTime)) / 8.00))
		remainingPercent -= smallSpikeGone
		odds.SmallSpike -= smallSpikeGone
		if uint16(prices[0])*2 > odds.MaxPrice {
			odds.MaxPrice = uint16(prices[0]) * 2
		}
		if uint16(math.Ceil(prices[0]*1.4)) < odds.MinPrice {
			odds.MinPrice = uint16(math.Ceil(prices[0] * 1.4))
		}
	}

	if !isRandom {
		remainingPercent -= odds.Random
		odds.Random = 0
	} else {
		if uint16(math.Ceil(prices[0]*1.4)) > odds.MaxPrice {
			odds.MaxPrice = uint16(math.Ceil(prices[0] * 1.4))
		}
		if uint16(math.Ceil(prices[0]*0.9)) < odds.MinPrice {
			odds.MinPrice = uint16(math.Ceil(prices[0] * 0.9))
		}
	}

	if !isDecreasing {
		remainingPercent -= odds.Decreasing
		odds.Decreasing = 0
	} else {
		if uint16(prices[1]) > odds.MaxPrice {
			odds.MaxPrice = uint16(prices[1])
		}
		if uint16(math.Ceil(prices[0]*0.5)) < odds.MinPrice {
			odds.MinPrice = uint16(math.Ceil(prices[0] * 0.5))
		}
	}

	if remainingPercent < 0.005 {
		return Odds{
			Unknown: true,
		}
	}

	odds.LargeSpike /= remainingPercent
	odds.SmallSpike /= remainingPercent
	odds.Random /= remainingPercent
	odds.Decreasing /= remainingPercent

	odds.LargeSpike = math.Round(odds.LargeSpike*10000) / 10000
	odds.SmallSpike = math.Round(odds.SmallSpike*10000) / 10000
	odds.Random = math.Round(odds.Random*10000) / 10000
	odds.Decreasing = math.Round(odds.Decreasing*10000) / 10000

	odds.AveragePrice = uint16(math.Ceil(odds.LargeSpike*prices[0]*4 + odds.SmallSpike*prices[0]*1.7 + odds.Random*prices[0]*1.15 + odds.Decreasing*prices[0]*0.65))

	if odds.LargeSpike == 1 {
		priceData.Pattern = LargeSpike.String()
		database.UpdatePrices(priceData)
	} else if odds.SmallSpike == 1 {
		priceData.Pattern = SmallSpike.String()
		database.UpdatePrices(priceData)
	} else if odds.Random == 1 {
		priceData.Pattern = Random.String()
		database.UpdatePrices(priceData)
	} else if odds.Decreasing == 1 {
		priceData.Pattern = Decreasing.String()
		database.UpdatePrices(priceData)
	}

	return odds
}

func OddsToString(odds Odds, spikeTime string, currentTime database.PriceTime) string {
	if odds.LargeSpike == 1 {
		if currentTime > database.GetPriceTimeFromCommand(spikeTime) {
			return "You have a Large Spike! Sadly, your best price is behind you."
		}
		return "You have a Large Spike! Wooo! Your best price will be on " + database.GetPriceTimeFromCommand(spikeTime).Message() + "!"
	}
	if odds.SmallSpike == 1 {
		bestPrice := database.GetPriceTimeFromCommand(spikeTime).Message()
		if bestPrice != "Unknown Unknown" {
			if currentTime > database.GetPriceTimeFromCommand(spikeTime) {
				return "You have a Small Spike! Sadly, your best price is behind you."
			}
			return "You have a Small Spike! Your best price will be on " + database.GetPriceTimeFromCommand(spikeTime).Message() + "!"
		}
		return "You have a Small Spike! The timing of your spike is still unknown."
	}
	if odds.Random == 1 {
		return "You have Random Town, USA.. 🎢 Weeeeee!!"
	}
	if odds.Decreasing == 1 {
		return "🎶 hello darkness my old friend 🎶"
	}

	bookie := "Tom Bookie's odds:"
	var oddStrings [4]string
	if odds.Unknown {
		bookie += "\nInvalid prices! Seek immediate help."
	}
	if odds.LargeSpike != 0 {
		oddStrings[odds.LargeSpikeRanking()] = "\nLarge Spike: " + strconv.FormatFloat(odds.LargeSpike*100, 'f', 0, 64) + "%"
	}
	if odds.SmallSpike != 0 {
		oddStrings[odds.SmallSpikeRanking()] = "\nSmall Spike: " + strconv.FormatFloat(odds.SmallSpike*100, 'f', 0, 64) + "%"
	}
	if odds.Random != 0 {
		oddStrings[odds.RandomRanking()] = "\nRandom: " + strconv.FormatFloat(odds.Random*100, 'f', 0, 64) + "%"
	}
	if odds.Decreasing != 0 {
		oddStrings[odds.DecreasingRanking()] = "\nDecreasing: " + strconv.FormatFloat(odds.Decreasing*100, 'f', 0, 64) + "%"
	}

	for _, oddString := range oddStrings {
		bookie += oddString
	}

	bookie += "\n\nAverage Price: " + strconv.FormatFloat(float64(odds.AveragePrice), 'f', 0, 64)
	bookie += "\nPrice Range: " + strconv.FormatFloat(float64(odds.MinPrice), 'f', 0, 64) + " - " + strconv.FormatFloat(float64(odds.MaxPrice), 'f', 0, 64)

	return bookie
}

func DefaultOdds(user database.User) Odds {
	switch GetPattern(database.GetLastWeeksPattern(user)) {
	case LargeSpike:
		return Odds{
			LargeSpike: 0.05,
			SmallSpike: 0.20,
			Random:     0.50,
			Decreasing: 0.25,
			Unknown:    false,
		}
	case SmallSpike:
		return Odds{
			LargeSpike: 0.25,
			SmallSpike: 0.15,
			Random:     0.45,
			Decreasing: 0.15,
			Unknown:    false,
		}
	case Random:
		return Odds{
			LargeSpike: 0.30,
			SmallSpike: 0.35,
			Random:     0.20,
			Decreasing: 0.15,
			Unknown:    false,
		}
	case Decreasing:
		return Odds{
			LargeSpike: 0.45,
			SmallSpike: 0.25,
			Random:     0.25,
			Decreasing: 0.05,
			Unknown:    false,
		}
	default:
		return Odds{
			LargeSpike: 0.2625,
			SmallSpike: 0.2375,
			Random:     0.35,
			Decreasing: 0.15,
			Unknown:    false,
		}
	}
}
