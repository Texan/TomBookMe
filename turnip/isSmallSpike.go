package turnip

import "math"

// IsSmallSpike determines if a week's prices could be a Small Spike
func IsSmallSpike(prices []float64) bool {
	rangeMin := [5]float64{0.9, 0.9, 1.4, 1.4, 1.4}
	rangeMax := [5]float64{1.4, 1.4, 2.0, 2.0, 2.0}

	purchase := prices[0]

	spikeStart := 0
	minRate := 0.4
	maxRate := 0.9
	forbiddenPrice := false

	for i, price := range prices {
		if i == 0 {
			// Invalid purchase range
			if purchase < 90 || purchase > 110 {
				return false
			}
			continue
		}

		if i == 1 && price == math.Ceil(0.9*purchase) {
			forbiddenPrice = true
			minRate = (price-1)/purchase - 0.05
			maxRate = price/purchase - 0.03
			continue
		}

		if spikeStart < 5 &&
			price >= math.Ceil(rangeMin[spikeStart]*purchase) &&
			price <= math.Ceil(rangeMax[spikeStart]*purchase) {
			spikeStart += 1
			if i == 2 && forbiddenPrice {
				spikeStart += 1
			}
			if spikeStart == 5 {
				minRate = 0.4
				maxRate = 0.9
			}
		} else if (spikeStart == 0 || spikeStart == 5) &&
			price >= math.Ceil(minRate*purchase) &&
			price <= math.Ceil(maxRate*purchase) {
			minRate = (price-1)/purchase - 0.05
			maxRate = price/purchase - 0.03

			// If we haven't spiked by Thursday PM, it's not a small spike
			if i == 8 && spikeStart == 0 {
				return false
			}
		} else {
			return false
		}
	}

	return true
}
