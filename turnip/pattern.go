package turnip

type Pattern int

const (
	Unknown Pattern = iota
	LargeSpike
	SmallSpike
	Random
	Decreasing
)

func (pattern Pattern) String() string {
	patterns := []string{
		"Unknown", "LargeSpike", "SmallSpike", "Random", "Decreasing",
	}

	if pattern < Unknown || pattern > Decreasing {
		return "Unknown"
	}

	return patterns[pattern]
}

func GetPattern(pattern string) Pattern {
	switch pattern {
	case "LargeSpike":
		return LargeSpike
	case "SmallSpike":
		return SmallSpike
	case "Random":
		return Random
	case "Decreasing":
		return Decreasing
	default:
		return Unknown
	}

}

func (pattern Pattern) Spike() bool {
	switch pattern {
	case LargeSpike, SmallSpike:
		return true
	default:
		return false
	}
}
