package turnip

import "math"

// IsDecreasing determines if a week's prices could be Decreasing
func IsDecreasing(prices []float64) bool {
	purchase := prices[0]
	minRate := 0.85
	maxRate := 0.9

	for i, price := range prices {
		if i == 0 {
			// Invalid purchase range
			if purchase < 90 || purchase > 110 {
				return false
			}
			continue
		}

		if price >= math.Ceil(minRate*purchase) &&
			price <= math.Ceil(maxRate*purchase) {
			minRate = (price-1)/purchase - 0.05
			maxRate = price/purchase - 0.03
		} else {
			return false
		}
	}

	return true
}
