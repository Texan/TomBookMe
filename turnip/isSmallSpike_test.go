package turnip_test

import (
	"testing"

	"gitlab.com/Texan/TomBookMe/turnip"
)

// Test that Small Spikes only start between Monday AM and Thursday PM
func Test_ValidSmallSpikesFromMondayAmToThursdayPm(t *testing.T) {
	tests := [][]float64{
		{100, 91, 112, 169, 187, 169, 73, 69, 65, 61, 57, 53, 49},
		{100, 89, 91, 112, 169, 187, 169, 73, 69, 65, 61, 57, 53},
		{100, 89, 85, 91, 112, 169, 187, 169, 73, 69, 65, 61, 57},
		{100, 89, 85, 81, 91, 112, 169, 187, 169, 73, 69, 65, 61},
		{100, 89, 85, 81, 77, 91, 112, 169, 187, 169, 73, 69, 65},
		{100, 89, 85, 81, 77, 73, 91, 112, 169, 187, 169, 73, 69},
		{100, 89, 85, 81, 77, 73, 69, 91, 112, 169, 187, 169, 73},
		{100, 89, 85, 81, 77, 73, 69, 65, 91, 112, 169, 187, 169},
	}

	for _, spike := range tests {
		result := turnip.IsSmallSpike(spike)
		if !result {
			t.Errorf("Test case '%v' failed, expected: '%t', got: '%t'", spike, true, result)
		}
	}
}

// Test that Small Spikes are valid mid-week
func Test_ValidPartialSmallSpikes(t *testing.T) {
	tests := [][]float64{
		{100},
		{100, 40},
		{100, 88},
		{100, 140},
		{103, 87},
		{103, 88},
		{103, 93},
		{103, 94},
		{100, 88, 84},
		{100, 59, 55},
		{100, 88, 84, 80},
		{100, 88, 84, 80, 76},
		{100, 88, 84, 80, 76, 72},
		{100, 88, 84, 80, 76, 72, 68},
		{100, 88, 84, 80, 76, 72, 68, 64},
		{100, 88, 84, 80, 76, 72, 68, 64, 119},
		{100, 50, 46, 42, 38, 34, 30, 26, 119, 132},
		{100, 88, 124, 130},
		{100, 88, 130, 124, 169},
		{100, 88, 130, 124, 169, 175, 169, 69},
	}

	for _, spike := range tests {
		result := turnip.IsSmallSpike(spike)
		if !result {
			t.Errorf("Test case '%v' failed, expected: '%t', got: '%t'", spike, true, result)
		}
	}
}

// Test that Small Spikes don't start between Friday AM and Saturday PM
func Test_InvalidSmallSpikesFromMondayAmToSaturdayPm(t *testing.T) {
	tests := [][]float64{
		{100, 124, 175, 405, 186, 119, 75, 69, 80, 56, 59, 64, 66},
		{100, 88, 84, 80, 76, 72, 68, 64, 60, 124, 132, 169, 186},
		{100, 88, 84, 80, 76, 72, 68, 64, 60, 56, 124, 132, 169},
		{100, 88, 84, 80, 76, 72, 68, 64, 60, 56, 52, 124, 132},
		{100, 88, 84, 80, 76, 72, 68, 64, 60, 56, 52, 48, 124},
	}

	for _, spike := range tests {
		result := turnip.IsSmallSpike(spike)
		if result {
			t.Errorf("Test case '%v' failed, expected: '%t', got: '%t'", spike, true, result)
		}
	}
}

// Test that Small Spikes are invalid mid-week
func Test_InvalidPartialSmallSpikes(t *testing.T) {
	tests := [][]float64{
		{120},
		{100, 39},
		{100, 141},
		{100, 88, 80},
		{100, 88, 84, 80, 170},
		{100, 88, 84, 80, 50},
		{100, 88, 84, 80, 76, 72, 68, 64, 60},
		{100, 88, 84, 80, 76, 72, 68, 64, 60, 119},
		{100, 88, 84, 119, 169},
		{100, 88, 84, 119, 80},
	}

	for _, spike := range tests {
		result := turnip.IsSmallSpike(spike)
		if result {
			t.Errorf("Test case '%v' failed, expected: '%t', got: '%t'", spike, true, result)
		}
	}
}

// Test that the Forbidden Price is valid
func Test_ValidForbiddenPrice(t *testing.T) {
	tests := [][]float64{
		{103},
		{103, 92},
		{103, 93},
		{103, 94},
		{103, 92, 88},
		{103, 93, 88},
		{103, 93, 88, 94, 112},
		{103, 93, 112},
		{103, 94, 112},
	}

	for _, spike := range tests {
		result := turnip.IsSmallSpike(spike)
		if !result {
			t.Errorf("Test case '%v' failed, expected: '%t', got: '%t'", spike, true, result)
		}
	}
}

// Test that the Forbidden Price is invalid
func Test_InvalidForbiddenPrice(t *testing.T) {
	tests := [][]float64{
		{103, 94, 91},
		{103, 93, 88, 112, 160},
		{103, 93, 95, 88},
		{103, 93, 112, 120},
	}

	for _, spike := range tests {
		result := turnip.IsSmallSpike(spike)
		if result {
			t.Errorf("Test case '%v' failed, expected: '%t', got: '%t'", spike, true, result)
		}
	}
}
