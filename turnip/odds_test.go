package turnip_test

import (
	"testing"

	"gitlab.com/Texan/TomBookMe/database"
	"gitlab.com/Texan/TomBookMe/turnip"
)

func Test_DefiniteLargeSpike(t *testing.T) {
	input := database.Prices{
		Purchase: 100,
		MonAM:    88,
		MonPM:    84,
		TueAM:    124,
		TuePM:    169,
	}
	result := turnip.CalculateOdds(&input)
	expected := turnip.Odds{
		LargeSpike:   1,
		AveragePrice: 400,
		MaxPrice:     600,
		MinPrice:     200,
	}
	if result != expected {
		t.Errorf("Test case '%v' failed, expected: '%v', got: '%v'", input, expected, result)
	}
}

func Test_DefiniteSmallSpike(t *testing.T) {
	input := database.Prices{
		Purchase: 100,
		MonAM:    88,
		MonPM:    84,
		TueAM:    124,
		TuePM:    139,
	}
	result := turnip.CalculateOdds(&input)
	expected := turnip.Odds{
		SmallSpike:   1,
		AveragePrice: 170,
		MaxPrice:     200,
		MinPrice:     140,
	}
	if result != expected {
		t.Errorf("Test case '%v' failed, expected: '%v', got: '%v'", input, expected, result)
	}
}

func Test_DefiniteRandom(t *testing.T) {
	input := database.Prices{
		Purchase: 100,
		MonAM:    110,
		MonPM:    123,
		TueAM:    138,
	}
	result := turnip.CalculateOdds(&input)
	expected := turnip.Odds{
		Random:       1,
		AveragePrice: 115,
		MaxPrice:     140,
		MinPrice:     90,
	}
	if result != expected {
		t.Errorf("Test case '%v' failed, expected: '%v', got: '%v'", input, expected, result)
	}
}

func Test_DefiniteDecreasing(t *testing.T) {
	input := database.Prices{
		Purchase: 100,
		MonAM:    88,
		MonPM:    84,
		TueAM:    80,
		TuePM:    76,
		WedAM:    72,
		WedPM:    69,
		ThuAM:    65,
		ThuPM:    61,
	}
	result := turnip.CalculateOdds(&input)
	expected := turnip.Odds{
		Decreasing:   1,
		AveragePrice: 65,
		MaxPrice:     88,
		MinPrice:     50,
	}
	if result != expected {
		t.Errorf("Test case '%v' failed, expected: '%v', got: '%v'", input, expected, result)
	}
}

func Test_NoDataYet(t *testing.T) {
	input := database.Prices{
		Purchase: 100,
	}
	result := turnip.CalculateOdds(&input)
	expected := turnip.Odds{
		LargeSpike: 0.2625,
		SmallSpike: 0.2375,
		Random:     0.35,
		Decreasing: 0.15,
		Unknown:    false,
	}
	if result != expected {
		t.Errorf("Test case '%v' failed, expected: '%v', got: '%v'", input, expected, result)
	}
}

func Test_DefiniteInvalid(t *testing.T) {
	input := database.Prices{
		Purchase: 100,
		MonAM:    88,
		MonPM:    84,
		TueAM:    80,
		TuePM:    124,
		WedAM:    178,
		WedPM:    69,
		ThuAM:    65,
		ThuPM:    61,
	}
	result := turnip.CalculateOdds(&input)
	expected := turnip.Odds{
		Unknown: true,
	}
	if result != expected {
		t.Errorf("Test case '%v' failed, expected: '%v', got: '%v'", input, expected, result)
	}
}

func Test_SmallSpikeLoweredOdds(t *testing.T) {
	input := database.Prices{
		Purchase: 100,
		MonAM:    86,
	}
	result := turnip.CalculateOdds(&input)
	expected := turnip.Odds{
		LargeSpike:   0.6058,
		SmallSpike:   0.048,
		Random:       0,
		Decreasing:   0.3462,
		AveragePrice: 273,
		MinPrice:     50,
		MaxPrice:     600,
		Unknown:      false,
	}
	if result != expected {
		t.Errorf("Test case '%v' failed, expected: '%v', got: '%v'", input, expected, result)
	}
}
