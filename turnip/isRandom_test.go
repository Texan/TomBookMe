package turnip_test

import (
	"testing"

	"gitlab.com/Texan/TomBookMe/turnip"
)

// Test that Random patterns are valid on the week
func Test_ValidRandomPatterns(t *testing.T) {
	tests := [][]float64{
		{100, 110, 124, 132, 98, 135, 111, 75, 69, 124, 64, 56, 49},
		{100, 76, 71, 124, 79, 74, 69, 119, 111, 139, 91, 123, 124},
	}

	for _, spike := range tests {
		result := turnip.IsRandom(spike)
		if !result {
			t.Errorf("Test case '%v' failed, expected: '%t', got: '%t'", spike, true, result)
		}
	}
}

// Test that Random patterns are valid mid-week
func Test_ValidPartialRandomPatterns(t *testing.T) {
	tests := [][]float64{
		{100},
		{100, 76},
		{103, 93},
		{103, 124},
		{100, 76, 69},
		{100, 76, 69, 61},
		{100, 76, 69, 61, 124},
		{100, 76, 69, 124, 76, 69},
		{100, 76, 69, 124, 76, 69, 61},
		{100, 76, 69, 124, 76, 69, 61, 124},
		{100, 76, 69, 124, 76, 69, 61, 124, 110},
		{100, 76, 69, 62, 124, 69, 62, 124, 110},
	}

	for _, spike := range tests {
		result := turnip.IsRandom(spike)
		if !result {
			t.Errorf("Test case '%v' failed, expected: '%t', got: '%t'", spike, true, result)
		}
	}
}

// Test that Random patterns aren't valid for the whole week
func Test_InvalidRandomPatterns(t *testing.T) {
	tests := [][]float64{
		{100, 124, 175, 405, 186, 119, 75, 69, 80, 56, 59, 64, 66},
		{100, 82, 75, 69, 123, 132, 68, 62, 124, 132, 111, 98, 125},
		{100, 79, 74, 69, 123, 132, 68, 62, 57, 132, 111, 98, 125},
		{100, 79, 74, 69, 61, 123, 132, 68, 121, 132, 111, 98, 125},
		{100, 121, 124, 111, 105, 98, 123, 134, 65, 58, 51, 45, 38},
	}

	for _, spike := range tests {
		result := turnip.IsRandom(spike)
		if result {
			t.Errorf("Test case '%v' failed, expected: '%t', got: '%t'", spike, true, result)
		}
	}
}

// Test that Random patterns are invalid mid-week
func Test_InvalidPartialRandomPatterns(t *testing.T) {
	tests := [][]float64{
		{120},
		{100, 81},
		{100, 141},
		{100, 59},
		{100, 89},
		{103, 87},
		{100, 79, 61},
		{100, 79, 71, 65, 141},
		{100, 79, 71, 65, 59},
		{100, 79, 71, 65, 132, 69, 61, 55},
		{100, 132, 111, 119, 124, 123, 109, 97},
		{100, 132, 111, 119, 124, 123, 109, 78, 72, 65, 98, 123},
	}

	for _, spike := range tests {
		result := turnip.IsRandom(spike)
		if result {
			t.Errorf("Test case '%v' failed, expected: '%t', got: '%t'", spike, true, result)
		}
	}
}
