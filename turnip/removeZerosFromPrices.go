package turnip

func RemoveZerosFromPrices(priceData []float64) []float64 {
	for i := len(priceData) - 1; i >= 0; i-- {
		if priceData[i] == 0 {
			priceData = priceData[:len(priceData)-1]
		} else {
			break
		}
	}
	return priceData
}
