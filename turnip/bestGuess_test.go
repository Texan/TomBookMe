package turnip_test

import (
	"testing"

	"gitlab.com/Texan/TomBookMe/turnip"
)

// Test that RemoveZerosFromPrices only removes trailing zeros
func Test_BestGuesses(t *testing.T) {
	tests := [][]float64{
		{93},
		{0, 87},
		{0, 93},
		{0, 107},
		{0, 65},
		{99, 0, 83},
		{99, 83, 0, 76},
		{99, 83, 80, 0, 73},
		{99, 83, 0, 0, 73},
		{99, 0, 0, 81},
		{99, 0, 0, 0, 78},
		{0, 0, 69},
		{0, 0, 0, 45},
		{0, 0, 0, 0, 0, 0, 0, 65},
		{100, 88, 83, 80, 0, 123},
		{100, 88, 83, 80, 0, 123, 132},
		{100, 88, 83, 80, 0, 123, 167, 178},
	}

	expected := [][]float64{
		{93},
		{100, 87},
		{107, 93},
		{94, 107},
		{100, 65},
		{99, 87, 83},
		{99, 83, 80, 76},
		{99, 83, 80, 77, 73},
		{99, 83, 79, 76, 73},
		{99, 87, 84, 81},
		{99, 87, 83, 81, 78},
		{100, 73, 69},
		{100, 53, 49, 45},
		{100, 88, 84, 80, 76, 72, 69, 65},
		{100, 88, 83, 80, 76, 123},
		{100, 88, 83, 80, 76, 123, 132},
		{100, 88, 83, 80, 122, 123, 167, 178},
	}

	for i, prices := range tests {
		result := turnip.BestGuess(prices)
		if len(result) != len(expected[i]) {
			t.Errorf("Test case '%v' failed, expected: '%v', got: '%v'", tests[i], expected[i], result)
		}
		for k, v := range result {
			if v != expected[i][k] {
				t.Errorf("Test case '%v' failed, expected: '%v', got: '%v'", tests[i], expected[i], result)
			}
		}
	}
}
