package turnip

import "math"

// IsLargeSpike determines if a week's prices could be a Large Spike
func IsLargeSpike(prices []float64) bool {
	rangeMin := [6]float64{0.90, 1.40, 2.00, 1.40, 0.90, 0.40}
	rangeMax := [6]float64{1.40, 2.00, 6.00, 2.00, 1.40, 0.90}

	purchase := prices[0]

	spikeStart := 0
	minRate := 0.85
	maxRate := 0.9
	for i, price := range prices {
		if i == 0 {
			// Invalid purchase range
			if purchase < 90 || purchase > 110 {
				return false
			}
			continue
		}

		if i != 1 &&
			price >= math.Ceil(rangeMin[spikeStart]*purchase) &&
			price <= math.Ceil(rangeMax[spikeStart]*purchase) {
			if spikeStart != 5 {
				spikeStart += 1
			}
		} else if spikeStart == 0 &&
			price >= math.Ceil(minRate*purchase) &&
			price <= math.Ceil(maxRate*purchase) {
			minRate = (price-1)/purchase - 0.05
			maxRate = price/purchase - 0.03

			// If we haven't spiked by Thursday PM, it's not a large spike
			if i == 8 {
				return false
			}
		} else {
			return false
		}
	}

	return true
}
