package turnip_test

import (
	"testing"

	"gitlab.com/Texan/TomBookMe/turnip"
)

// Test that Large Spikes only start between Monday PM and Thursday PM
func Test_ValidLargeSpikesFromMondayPmToThursdayPm(t *testing.T) {
	tests := [][]float64{
		{100, 88, 124, 175, 405, 186, 119, 75, 69, 80, 56, 59, 64},
		{100, 88, 84, 124, 175, 405, 186, 119, 75, 69, 80, 56, 59},
		{100, 88, 84, 80, 124, 175, 405, 186, 119, 75, 69, 80, 56},
		{100, 88, 84, 80, 76, 124, 175, 405, 186, 119, 75, 69, 80},
		{100, 88, 84, 80, 76, 72, 124, 175, 405, 186, 119, 75, 69},
		{100, 88, 84, 80, 76, 72, 68, 124, 175, 405, 186, 119, 75},
		{100, 88, 84, 80, 76, 72, 68, 64, 124, 175, 405, 186, 119},
	}

	for _, spike := range tests {
		result := turnip.IsLargeSpike(spike)
		if !result {
			t.Errorf("Test case '%v' failed, expected: '%t', got: '%t'", spike, true, result)
		}
	}
}

// Test that Large Spikes are valid mid-week
func Test_ValidPartialLargeSpikes(t *testing.T) {
	tests := [][]float64{
		{100},
		{100, 88},
		{103, 88},
		{103, 93},
		{100, 88, 84},
		{100, 88, 84, 80},
		{100, 88, 84, 80, 76},
		{100, 88, 84, 80, 76, 72},
		{100, 88, 84, 80, 76, 72, 68},
		{100, 88, 84, 80, 76, 72, 68, 64},
		{100, 88, 84, 80, 76, 72, 68, 64, 119},
		{100, 88, 124, 175},
		{100, 88, 124, 175, 500},
		{100, 88, 124, 175, 500, 175, 124, 69},
	}

	for _, spike := range tests {
		result := turnip.IsLargeSpike(spike)
		if !result {
			t.Errorf("Test case '%v' failed, expected: '%t', got: '%t'", spike, true, result)
		}
	}
}

// Test that Large Spikes don't start on Monday AM or between Friday AM and Saturday PM
func Test_InvalidLargeSpikesFromMondayAmToSaturdayPm(t *testing.T) {
	tests := [][]float64{
		{100, 124, 175, 405, 186, 119, 75, 69, 80, 56, 59, 64, 66},
		{100, 88, 84, 80, 76, 72, 68, 64, 60, 124, 175, 405, 186},
		{100, 88, 84, 80, 76, 72, 68, 64, 60, 56, 124, 175, 405},
		{100, 88, 84, 80, 76, 72, 68, 64, 60, 56, 52, 124, 175},
		{100, 88, 84, 80, 76, 72, 68, 64, 60, 56, 52, 48, 124},
	}

	for _, spike := range tests {
		result := turnip.IsLargeSpike(spike)
		if result {
			t.Errorf("Test case '%v' failed, expected: '%t', got: '%t'", spike, true, result)
		}
	}
}

// Test that Large Spikes are invalid mid-week
func Test_InvalidPartialLargeSpikes(t *testing.T) {
	tests := [][]float64{
		{120},
		{100, 84},
		{100, 91},
		{103, 87},
		{100, 88, 80},
		{100, 88, 84, 80, 170},
		{100, 88, 84, 80, 50},
		{100, 88, 84, 80, 76, 72, 68, 64, 60},
		{100, 88, 84, 80, 76, 72, 68, 64, 60, 119},
		{100, 88, 84, 119, 124},
		{100, 88, 84, 119, 80},
	}

	for _, spike := range tests {
		result := turnip.IsLargeSpike(spike)
		if result {
			t.Errorf("Test case '%v' failed, expected: '%t', got: '%t'", spike, true, result)
		}
	}
}
