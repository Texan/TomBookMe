package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/viper"
)

func init() {
	env := os.Getenv("BOT_ENV")
	if env == "" {
		env = "qa"
	}
	viper.SetConfigName(strings.ToLower(env))
	viper.AddConfigPath("conf/")
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("fatal error config file: %s \n", err))
	}
}
