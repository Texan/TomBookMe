package message

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

type messageHandler struct {
	Token string
}

// MessageHandler creates a new messageHandler with a given token
func MessageHandler(token string) *messageHandler {
	mh := new(messageHandler)
	mh.Token = token
	return mh
}

// SendMessage takes a string and posts it to GroupMe with Tom Bookie
func (mh *messageHandler) SendMessage(message string) {
	if mh.Token == "" {
		fmt.Printf("%v\n", message)
	} else {
		data := Payload{
			mh.Token, message,
		}
		payloadBytes, err := json.Marshal(data)
		if err != nil {
			// handle err
		}
		body := bytes.NewReader(payloadBytes)

		req, err := http.NewRequest("POST", "https://api.groupme.com/v3/bots/post", body)
		if err != nil {
			// handle err
		}
		req.Header.Set("Content-Type", "application/json")

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			// handle err
		}
		defer resp.Body.Close()
	}
}
