package message

type Message struct {
	CreatedAt int64  `json:"created_at"`
	GroupID   string `json:"group_id"`
	Id        string `json:"id"`
	Name      string `json:"name"`
	SenderID  string `json:"sender_id"`
	Text      string `json:"text"`
	UserID    string `json:"user_id"`
}

func (m *Message) ToString() string {
	return "Group: " + m.GroupID + "\nName: " + m.Name + "\nSendID: " + m.SenderID + "\nUserID: " + m.UserID + "\nText: " + m.Text
}
