package message

type Payload struct {
	BotID string `json:"bot_id"`
	Text  string `json:"text"`
}
