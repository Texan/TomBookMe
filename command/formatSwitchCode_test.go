package command_test

import (
	"testing"

	"gitlab.com/Texan/TomBookMe/command"
)

// Test that makes sure switch codes are properly formatted
func Test_VariousValidSwitchCodeFormattings(t *testing.T) {
	tests := []string{
		"SW123456781234",
		"SW-1234-5678-1234",
		"sw-1234-5678-1234",
		"sw123456781234",
		"123456781234",
		"1234-5678-1234",
		"1234 5678 1234",
		"SW 1234 5678 1234",
	}

	expected := []string{
		"SW-1234-5678-1234",
		"SW-1234-5678-1234",
		"SW-1234-5678-1234",
		"SW-1234-5678-1234",
		"SW-1234-5678-1234",
		"SW-1234-5678-1234",
		"SW-1234-5678-1234",
		"SW-1234-5678-1234",
	}

	for i, code := range tests {
		result := command.FormatSwitchCode(code)
		if result != expected[i] {
			t.Errorf("Test case '%s' failed, expected: '%s', got: '%s'", code, expected[i], result)
		}
	}
}

// Test that makes sure switch codes that aren't right aren't added
func Test_VariousInvalidSwitchCodeFormattings(t *testing.T) {
	tests := []string{
		"SW12345678123",
		"SW-1234-5678-123",
		"sw-1234-5678-12343",
		"sw1234567812314",
		"sw12345678314",
		"12345678234",
		"1234567338234",
		"1234-5678-12344",
		"1234 56478 1234",
		"SW 12234 5678 1234",
		"SW 134 5678 1234",
		"1234 568 12343",
	}

	expected := "Invalid Switch code! Please try again."

	for _, code := range tests {
		result := command.FormatSwitchCode(code)
		if result != expected {
			t.Errorf("Test case '%s' failed, expected: '%s', got: '%s'", code, expected, result)
		}
	}
}
