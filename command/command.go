package command

import (
	"bufio"
	"math"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
	"unicode/utf8"

	"gitlab.com/Texan/TomBookMe/database"
	"gitlab.com/Texan/TomBookMe/message"
	"gitlab.com/Texan/TomBookMe/turnip"
)

func ProcessCommand(m message.Message) string {
	text := regexp.MustCompile("\\s").Split(strings.TrimSpace(trimFirstRune(m.Text)), 2)
	command := strings.ToLower(text[0])
	var argument string
	if len(text) > 1 {
		argument = strings.ToLower(text[1])
	}

	// Parses the -u flag to see if we're using a secondary user
	var name string
	if argument != "" {
		userToggle, _ := regexp.Compile("-u")
		if userToggle.MatchString(argument) {
			name = strings.Title(strings.Split(argument, "-u ")[1])
			argument = strings.TrimSpace(strings.Split(argument, "-u ")[0])
		}
	}

	// Give a blank command the price command and move the price to the argument
	regPrice, _ := regexp.Compile("[0-9][0-9]")
	if regPrice.MatchString(command) {
		argument = command
		command = "price"
	}

	// Find, or create, the user or secondary user
	user := database.GetUser(m.UserID + name)
	if user.ID == 0 {
		if name != "" {
			database.AddUser(name, m.UserID+name)
			user = database.GetUser(m.UserID + name)
		} else {
			name = strings.Title(strings.Split(user.Name, " ")[0])
			if name == "" {
				name = m.Name
			}
			database.AddUser(name, m.UserID)
			user = database.GetUser(m.UserID)
		}
	}

	if name == "" {
		name = strings.Title(strings.Split(user.Name, " ")[0])
	}

	price := TextToUint(argument)
	prices := CurrentPrices(user)
	currentTime := database.GetPriceTimeFromCommand(command)
	if currentTime == -1 {
		currentTime = CurrentPriceTime(user)
	} else {
		command = "price"
	}

	switch command {
	case "am":
		currentTime = CurrentAMTime(currentTime)
		command = "price"
	case "pm":
		currentTime = CurrentPMTime(currentTime)
		command = "price"
	}

	switch command {
	case "price":
		if argument != "" {
			prices.SetPrice(currentTime, price)
			database.UpdatePrices(&prices)
			odds := turnip.CalculateOdds(&prices)

			message := name + "'s " + currentTime.Message() + " price was set to: " + argument
			if currentTime == database.Purchase {
				return message
			}
			oddsMessage := turnip.OddsToString(odds, prices.BestPrice(), currentTime)
			if strings.Contains(oddsMessage, currentTime.Message()) &&
				strings.Contains(oddsMessage, "Spike") {
				smallLarge := "small"
				if strings.Contains(oddsMessage, "Large Spike") {
					smallLarge = "large"
				}
				userTime, _ := time.LoadLocation(user.TimeZone)
				userNow := time.Now().In(userTime)
				oddsMessage = "This is the peak of your " + smallLarge + " spike!\n" +
					"There's a " + strconv.FormatFloat(BestPriceThisWeek(price, user.UserID, userNow), 'f', 2, 64) + "%" + " chance this will be the best price this week!"
			}
			return message + "\n\n" + oddsMessage
		} else {
			return "Please enter a valid price."
		}
	case "gottagofast", "roomforjesus":
		return "https://www.youtube.com/watch?v=WjzAy4iVz2M"
	case "cheesewheel":
		return "https://www.youtube.com/watch?v=XJkfQekBNHI"
	case "killtomnook", "tomnookisacapitalistfraud":
		return "https://www.youtube.com/watch?v=xWC0BPznvRA"
	case "howtocook", "zeldarecipes":
		return "https://www.youtube.com/watch?v=JaZpmHC3LRg"
	case "wahhh", "stupidsexythanos":
		return "https://pbs.twimg.com/media/Ec7fmLpU0AEI4MD?format=jpg&name=4096x4096"
	case "help":
		return HelpCommand()
	case "odds", "bookie":
		return turnip.OddsToString(turnip.CalculateOdds(&prices), prices.BestPrice(), currentTime)
	case "pog":
		return SpikeView(time.Now(), true)
	case "prices":
		return prices.ToString()
	case "sc", "fc", "friend", "switch", "code":
		if argument != "" {
			fc := FormatSwitchCode(argument)
			if fc == "Invalid Switch code! Please try again." {
				return fc
			}
			database.AddUserSwitchCode(user, fc)
			if name != "" {
				return name + "'s friend code updated to: " + fc
			}
			return "Friend code updated to: " + fc
		} else {
			return "Your friend code is: " + user.SwitchCode
		}
	case "tz":
		if argument != "" {
			var tz string
			switch strings.ToLower(argument) {
			case "kst", "kdt", "korea", "seoul", "asia/seoul":
				tz = "Asia/Seoul"
			case "est", "edt", "eastern", "east", "new york", "america/new york", "america/new_york":
				tz = "America/New_York"
			case "cst", "cdt", "central", "chicago", "texas", "austin", "america/chicago":
				tz = "America/Chicago"
			case "mst", "mdt", "mountain", "denver", "america/denver":
				tz = "America/Denver"
			case "phoenix", "arizona", "america/phoenix":
				tz = "America/Phoenix"
			case "pst", "pdt", "pacific", "california", "los angeles", "america/los angeles":
				tz = "America/Vancouver"
			case "ast", "adt", "alaska", "anchorage", "america/anchorage":
				tz = "America/Anchorage"
			default:
				tz = text[1]
			}

			database.AddUserTimeZone(user, tz)
			if name != "" {
				return name + "'s time zone updated to: " + tz
			}
			return "Time zone updated to: " + tz
		} else {
			return "Your current time zone is: " + user.TimeZone
		}
	}

	return "Invalid command! Please type $help for more info."
}

func CurrentPrices(user database.User) database.Prices {
	prices := database.GetPrices(user, database.GetSunday(user, true))
	if prices.ID == 0 {
		lastWeek := database.GetPrices(user, database.GetSunday(user, false))
		if lastWeek.ID == 0 {
			database.AddPrices(user, database.GetSunday(user, false))
		}
		database.AddPrices(user, database.GetSunday(user, true))
		prices = database.GetPrices(user, database.GetSunday(user, true))
	}
	return prices
}

func CurrentUserTime(user database.User) time.Time {
	timeZone, _ := time.LoadLocation(user.TimeZone)
	return time.Now().In(timeZone)
}

func CurrentPriceTime(user database.User) database.PriceTime {
	now := CurrentUserTime(user)
	if now.Weekday() == 0 {
		return database.Purchase
	}

	offset := 0
	if now.Hour() < 6 {
		offset = -1
	} else if now.Hour() >= 12 {
		offset = 1
	}

	return database.PriceTime(int(now.Weekday())*2 - 1 + offset)
}

func CurrentAMTime(time database.PriceTime) database.PriceTime {
	if time.TimeOfDay() == "evening" {
		return time - 1
	}
	return time
}

func CurrentPMTime(time database.PriceTime) database.PriceTime {
	if time.TimeOfDay() == "morning" && time != database.Purchase {
		return time + 1
	}
	return time
}

func trimFirstRune(s string) string {
	_, i := utf8.DecodeRuneInString(s)
	return s[i:]
}

func TextToUint(text string) uint16 {
	price64, _ := strconv.ParseUint(text, 10, 16)
	return uint16(price64)
}

func GetVersion(fullNotes bool) string {
	file, err := os.Open("CHANGELOG.md")

	if err != nil {
		return ""
	}

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	var txtlines []string

	for scanner.Scan() {
		txtlines = append(txtlines, scanner.Text())
	}

	file.Close()

	if !fullNotes {
		return strings.Split(txtlines[0], " ")[1]
	}

	notes := ""
	for i, line := range txtlines {
		if i == 0 {
			notes += "New Release: " + strings.Split(line, " ")[1] + "\n"
			continue
		}
		if line != "" {
			notes += "\n" + line
		} else {
			break
		}
	}
	return notes
}

func BestPriceThisWeek(price uint16, user string, time time.Time) float64 {
	better := float64(1)
	thisWeek := database.GetWeeksPrices(database.GetSundayFromTime(time))
	for _, p := range thisWeek {
		if user != p.User && !StalePrice(time, p) {
			odds := turnip.CalculateOdds(&p)
			if odds.LargeSpike > 0 && !AlreadySpiked(time, p) {
				min := p.Purchase * 2
				average := p.Purchase * 4

				var compare float64
				if int(price)-int(min) <= 0 {
					compare = 1
				} else {
					compare = float64(price-min) / float64(average)
					if compare >= 1 {
						compare = 0
					} else {
						compare = 1 - compare
					}
				}
				better *= (1 - (compare * float64(odds.LargeSpike)))
			}
			if odds.SmallSpike > 0 && !AlreadySpiked(time, p) {
				min := float64(p.Purchase) * 1.4
				average := float64(p.Purchase) * 0.6

				var compare float64
				if float64(price)-float64(min) <= 0 {
					compare = 1
				} else {
					compare = (float64(price) - min) / float64(average)
					if compare >= 1 {
						compare = 0
					} else {
						compare = 1 - compare
					}
				}
				better *= (1 - (compare * float64(odds.SmallSpike)))
			}
		}
	}

	return math.Round(better*10000) / 100
}
