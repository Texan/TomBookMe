package command

func HelpCommand() string {
	para := "\n\n"
	newLine := "\n\t"

	return "Tom Bookie: A GroupMe Turnip Wizard \n" +
		"Version: " + GetVersion(false) + "\n" +
		"Usage: $[command] [arguments] {switches}" +
		para + "Prices:" +
		newLine + "[currentPrice]   (Ex: $100)" +
		newLine + "am [amPrice]    (Ex: $am 85)" +
		newLine + "pm [pmPrice]    (Ex: $pm 80)" +
		para + "Settings:" +
		newLine + "tz [timezone]     (Ex: $tz Pacific)" +
		newLine + "sc [switchcode] (Ex: $sc 123412341234)" +
		para + "Switches:" +
		newLine + "-u [name]   (Ex: $am 85 -u Tom)" +
		para + "Views:" +

		newLine + "$odds   (displays your current odds)" +
		newLine + "$pog     (displays spike percentages)" +
		newLine + "$prices (displays your prices)"
}
