package command_test

import (
	"testing"
	"time"

	"gitlab.com/Texan/TomBookMe/command"
	"gitlab.com/Texan/TomBookMe/database"
)

func Test_LargeSpikes(t *testing.T) {
	time := time.Date(2019, time.April, 9, 23, 0, 0, 0, time.UTC)
	AddPrices([]uint16{100, 85, 124, 176}, database.User{Name: "FirstSpike"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{100, 85, 124}, database.User{Name: "SecondSpike"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{100, 60, 56}, database.User{Name: "ThirdSpike"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{100, 85, 81, 124, 176}, database.User{Name: "FourthSpike"}, database.GetSundayFromTime(time))
	result := command.SpikeView(time, true)
	expected := "Ongoing Large Spikes\nFirstSpike\n\nUpcoming Large Spikes\nFourthSpike: Wednesday morning\n\nLarge Spike Odds (93%)\nSecondSpike: 93%"
	if result != expected {
		t.Errorf("Test case failed, expected: '%s', got: '%s'", expected, result)
	}
}

func Test_LargeSpikesSortNames(t *testing.T) {
	time := time.Date(2019, time.April, 23, 23, 0, 0, 0, time.UTC)
	AddPrices([]uint16{100, 85, 124, 176}, database.User{Name: "FirstSpike"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{100, 85, 124, 176}, database.User{Name: "SecondSpike"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{100, 85, 124, 176}, database.User{Name: "FourthSpike"}, database.GetSundayFromTime(time))
	result := command.SpikeView(time, true)
	expected := "Ongoing Large Spikes\nFirstSpike\nFourthSpike\nSecondSpike"
	if result != expected {
		t.Errorf("Test case failed, expected: '%s', got: '%s'", expected, result)
	}
}

func Test_LargeSpikesSortPrices(t *testing.T) {
	time := time.Date(2019, time.April, 23, 23, 0, 0, 0, time.UTC)
	AddPrices([]uint16{100, 85, 124, 176, 340}, database.User{Name: "FirstSpike"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{100, 85, 124, 176, 450}, database.User{Name: "SecondSpike"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{100, 85, 124, 176, 500}, database.User{Name: "FourthSpike"}, database.GetSundayFromTime(time))
	result := command.SpikeView(time, true)
	expected := "Ongoing Large Spikes\nFourthSpike: 500\nSecondSpike: 450\nFirstSpike: 340"
	if result != expected {
		t.Errorf("Test case failed, expected: '%s', got: '%s'", expected, result)
	}
}

func Test_LargeSpikesRemoveStaleUsersEvening(t *testing.T) {
	time := time.Date(2020, time.March, 4, 18, 0, 0, 0, time.UTC)
	AddPrices([]uint16{100, 85}, database.User{Name: "FirstSpike"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{100, 85, 124}, database.User{Name: "SecondSpike"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{100, 85, 80, 76}, database.User{Name: "ThirdSpike"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{100, 85, 80, 76, 98}, database.User{Name: "FourthSpike"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{100, 85, 80, 76, 72, 69, 65}, database.User{Name: "FifthSpike"}, database.GetSundayFromTime(time))
	result := command.SpikeView(time, true)
	expected := "Large Spike Odds (95%)\nFourthSpike: 93%\nFifthSpike: 32%"
	if result != expected {
		t.Errorf("Test case failed, expected: '%s', got: '%s'", expected, result)
	}
}

func Test_LargeSpikesNoSpikesSpikesFinished(t *testing.T) {
	time := time.Date(2020, time.February, 21, 18, 0, 0, 0, time.UTC)
	AddPrices([]uint16{100, 86, 124, 176, 400, 175, 124, 67, 76, 54, 69}, database.User{Name: "LargeSpikeTuesday"}, database.GetSundayFromTime(time))
	result := command.SpikeView(time, true)
	expected := "No more large or small spikes this week. F"
	if result != expected {
		t.Errorf("Test case failed, expected: '%s', got: '%s'", expected, result)
	}
}

func Test_LargeSpikesNoSpikes(t *testing.T) {
	time := time.Date(2020, time.February, 4, 10, 0, 0, 0, time.UTC)
	AddPrices([]uint16{100, 60}, database.User{Name: "FirstSpike"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{100, 60, 124}, database.User{Name: "SecondSpike"}, database.GetSundayFromTime(time))
	result := command.SpikeView(time, true)
	expected := "No more large spikes this week.\n\nSmall Spike Odds (33%)\nFirstSpike: 19%\nSecondSpike: 17%"
	if result != expected {
		t.Errorf("Test case failed, expected: '%s', got: '%s'", expected, result)
	}
}

func Test_LargeSpikesNoSpikes_ShowSmallSpikes(t *testing.T) {
	time := time.Date(2020, time.February, 26, 18, 0, 0, 0, time.UTC)
	AddPrices([]uint16{100, 60}, database.User{Name: "FirstSpike"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{100, 60, 56, 52, 124, 112, 175}, database.User{Name: "SecondSpike"}, database.GetSundayFromTime(time))
	result := command.SpikeView(time, true)
	expected := "No more large spikes this week.\n\nUpcoming Small Spikes\nSecondSpike: Thursday morning"
	if result != expected {
		t.Errorf("Test case failed, expected: '%s', got: '%s'", expected, result)
	}
}

func Test_ComboView(t *testing.T) {
	time := time.Date(2019, time.December, 25, 18, 0, 0, 0, time.UTC)
	AddPrices([]uint16{100, 50, 45, 111, 124, 160, 175}, database.User{Name: "SmallSpikeNow"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{100, 60, 56, 52, 124, 112, 175}, database.User{Name: "SmallSpikeSoon"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{100, 60, 56, 52, 48, 44, 40}, database.User{Name: "SmallSpikeUnknown"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{100, 87, 83, 79, 75, 71, 67}, database.User{Name: "SmallSpikeMaybe"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{100, 87, 83, 79, 124, 176, 400}, database.User{Name: "LargeSpikeNow"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{100, 87, 83, 79, 75, 124, 176}, database.User{Name: "LargeSpikeSoon"}, database.GetSundayFromTime(time))
	result := command.SpikeView(time, true)
	expected := "Ongoing Large Spikes\nLargeSpikeNow: 400\n\nUpcoming Large Spikes\nLargeSpikeSoon: Thursday morning\n\nLarge Spike Odds (32%)\nSmallSpikeMaybe: 32%\n\nOngoing Small Spikes\nSmallSpikeNow: 175\n\nUpcoming Small Spikes\nSmallSpikeSoon: Thursday morning\n\nGuaranteed Small Spikes\nSmallSpikeUnknown\n\nSmall Spike Odds (3%)\nSmallSpikeMaybe: 3%"
	if result != expected {
		t.Errorf("Test case failed, expected: '%s', got: '%s'", expected, result)
	}
}
