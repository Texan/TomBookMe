package command

import (
	"unicode"

	"gitlab.com/Texan/TomBookMe/database"
)

type SpikeOdds struct {
	Name    string
	Percent float64
	Price   uint16
	Time    database.PriceTime
}

type Spikes []SpikeOdds

func (a Spikes) Len() int { return len(a) }
func (a Spikes) Less(i, j int) bool {
	if a[j].Percent < a[i].Percent {
		return true
	} else if a[j].Percent == a[i].Percent {
		if a[i].Time == -1 && a[j].Time > 0 {
			return false
		} else if a[j].Time == -1 && a[i].Time > 0 {
			return true
		} else if a[i].Time < a[j].Time {
			return true
		} else if a[i].Time == a[j].Time {
			if a[j].Price < a[i].Price {
				return true
			} else if a[j].Price == a[i].Price {
				iRunes := []rune(a[i].Name)
				jRunes := []rune(a[j].Name)

				max := len(iRunes)
				if max > len(jRunes) {
					max = len(jRunes)
				}

				for idx := 0; idx < max; idx++ {
					ir := iRunes[idx]
					jr := jRunes[idx]

					lir := unicode.ToLower(ir)
					ljr := unicode.ToLower(jr)

					if lir != ljr {
						return lir < ljr
					}

					// the lowercase runes are the same, so compare the original
					if ir != jr {
						return ir < jr
					}
				}

				return false
			} else {
				return false
			}
		} else {
			return false
		}
	} else {
		return false
	}
}
func (a Spikes) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
