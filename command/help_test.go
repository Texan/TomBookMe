package command_test

import (
	"strings"
	"testing"

	"gitlab.com/Texan/TomBookMe/command"
)

func Test_HelpCommand(t *testing.T) {
	input := CreateMessage("$help", "HelpCommand")
	result := command.ProcessCommand(input)
	expected := "A GroupMe Turnip Wizard"
	if !strings.Contains(result, expected) {
		t.Errorf("Test case '%v' failed, expected: '%s', got: '%s'", input, expected+" to be in the response", result)
	}
}
