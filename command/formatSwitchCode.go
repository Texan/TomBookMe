package command

import (
	"regexp"
	"strings"
)

func FormatSwitchCode(switchCode string) string {
	r, _ := regexp.Compile("sw[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]")
	if r.MatchString(strings.ToLower(switchCode)) && len(switchCode) == 14 {
		return "SW-" + switchCode[2:6] + "-" + switchCode[6:10] + "-" + switchCode[10:14]
	}

	r, _ = regexp.Compile("sw-[0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]")
	if r.MatchString(strings.ToLower(switchCode)) && len(switchCode) == 17 {
		return "SW-" + switchCode[3:17]
	}

	r, _ = regexp.Compile("[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]")
	if r.MatchString(strings.ToLower(switchCode)) && len(switchCode) == 12 {
		return "SW-" + switchCode[0:4] + "-" + switchCode[4:8] + "-" + switchCode[8:12]
	}

	r, _ = regexp.Compile("[0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]")
	if r.MatchString(strings.ToLower(switchCode)) && len(switchCode) == 14 {
		return "SW-" + switchCode[0:14]
	}

	r, _ = regexp.Compile("sw [0-9][0-9][0-9][0-9] [0-9][0-9][0-9][0-9] [0-9][0-9][0-9][0-9]")
	if r.MatchString(strings.ToLower(switchCode)) && len(switchCode) == 17 {
		return "SW-" + switchCode[3:7] + "-" + switchCode[8:12] + "-" + switchCode[13:17]
	}

	r, _ = regexp.Compile("[0-9][0-9][0-9][0-9] [0-9][0-9][0-9][0-9] [0-9][0-9][0-9][0-9]")
	if r.MatchString(strings.ToLower(switchCode)) && len(switchCode) == 14 {
		return "SW-" + switchCode[0:4] + "-" + switchCode[5:9] + "-" + switchCode[10:14]
	}

	return "Invalid Switch code! Please try again."
}
