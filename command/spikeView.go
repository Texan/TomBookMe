package command

import (
	"math"
	"sort"
	"strconv"
	"strings"
	"time"

	"gitlab.com/Texan/TomBookMe/database"
	"gitlab.com/Texan/TomBookMe/turnip"
)

func StalePrice(currentTime time.Time, prices database.Prices) bool {
	priceTime := database.GetPriceTimeFromTime(currentTime)

	return priceTime >= database.TueAM &&
		prices.GetPrice(priceTime-2) == 0 && prices.GetPrice(priceTime-1) == 0 &&
		prices.GetPrice(priceTime) == 0
}

func AlreadySpiked(currentTime time.Time, prices database.Prices) bool {
	priceTime := database.GetPriceTimeFromTime(currentTime)
	spikeTime := database.GetPriceTimeFromCommand(prices.BestPrice())

	return spikeTime != -1 && spikeTime < priceTime
}

func SpikeView(time time.Time, largeSpike bool) string {
	thisWeek := database.GetWeeksPrices(database.GetSundayFromTime(time))
	peeps := make([]SpikeOdds, len(thisWeek))
	for i, p := range thisWeek {
		if !StalePrice(time, p) {
			odds := turnip.CalculateOdds(&p)
			spikeOdds := odds.LargeSpike
			if !largeSpike {
				spikeOdds = odds.SmallSpike
			}
			if spikeOdds > 0 && !AlreadySpiked(time, p) {
				peeps[i] = SpikeOdds{Name: database.GetName(p.User), Percent: math.Round(spikeOdds*100) / 100, Price: p.GetPrice(database.GetPriceTimeFromTime(time)), Time: database.GetPriceTimeFromCommand(p.BestPrice())}
			}
		}
	}
	sort.Sort(Spikes(peeps))

	totalOdds := float64(1)
	smallLarge := "Large"
	if !largeSpike {
		smallLarge = "Small"
	}
	var output string
	for _, peep := range peeps {
		if peep.Percent != 0 {
			if peep.Time == database.GetPriceTimeFromTime(time) {
				if !strings.Contains(output, "Ongoing "+smallLarge+" Spikes") {
					output += "Ongoing " + smallLarge + " Spikes"
				}
				output += "\n" + peep.Name
				if peep.Price != 0 {
					output += ": " + strconv.FormatFloat(float64(peep.Price), 'f', 0, 64)
				}
			} else if peep.Time.Message() != "Unknown Unknown" {
				if !strings.Contains(output, "Upcoming "+smallLarge+" Spikes") {
					if output != "" {
						output += "\n\n"
					}
					output += "Upcoming " + smallLarge + " Spikes"
				}
				output += "\n" + peep.Name + ": " + peep.Time.Message()
			} else if peep.Percent == 1 {
				if !strings.Contains(output, "Guaranteed "+smallLarge+" Spikes") {
					if output != "" {
						output += "\n\n"
					}
					output += "Guaranteed " + smallLarge + " Spikes"
				}
				output += "\n" + peep.Name
			} else {
				if !strings.Contains(output, smallLarge+" Spike Odds") {
					if output != "" {
						output += "\n\n"
					}
					output += smallLarge + " Spike Odds"
				}
				output += "\n" + peep.Name + ": " + strconv.FormatFloat(peep.Percent*100, 'f', 0, 64) + "%"
				totalOdds *= (1 - peep.Percent)
			}
		}
	}
	totalOdds = (1 - totalOdds) * 100
	oddsRounded := strconv.FormatFloat(totalOdds, 'f', 0, 64)
	if totalOdds > 99 {
		oddsRounded = strconv.FormatFloat(totalOdds, 'f', 2, 64)
	}
	output = strings.Replace(output, smallLarge+" Spike Odds", smallLarge+" Spike Odds ("+oddsRounded+"%)", 1)
	if output == "" {
		if smallLarge == "Large" {
			smallSpikes := SpikeView(time, false)
			if smallSpikes == "No more small spikes this week." {
				return "No more large or small spikes this week. F"
			}
			return "No more large spikes this week.\n\n" + smallSpikes
		}
		return "No more " + strings.ToLower(smallLarge) + " spikes this week."
	}
	if smallLarge == "Large" && totalOdds <= 80 {
		smallSpikes := SpikeView(time, false)
		if smallSpikes != "No more small spikes this week." {
			output += "\n\n" + smallSpikes
		}
	}
	return output
}
