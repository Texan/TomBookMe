package command_test

import (
	"os"
	"strings"
	"testing"
	"time"

	"gitlab.com/Texan/TomBookMe/command"
	"gitlab.com/Texan/TomBookMe/database"
	"gitlab.com/Texan/TomBookMe/message"
)

func TestMain(m *testing.M) {
	database.Connect("/tmp/turnip_test.db")
	testRunner := m.Run()
	database.DB.DropTable(&database.Prices{})
	database.DB.DropTable(&database.User{})
	os.Exit(testRunner)
}

func CreateMessage(text string, name string) message.Message {
	return message.Message{
		Name:   name,
		Text:   text,
		UserID: name,
	}
}

func SendCommands(commands []string, name string) string {
	var response string
	for _, c := range commands {
		response = command.ProcessCommand(CreateMessage("$"+c, name))
	}
	return response
}

func AddPrices(priceData []uint16, user database.User, week string) {
	database.AddUser(user.Name, user.Name)
	newUser := database.GetUser(user.Name)
	prices := database.GetPrices(newUser, week)
	for i, price := range priceData {
		prices.SetPrice(database.PriceTime(i), price)
	}
	database.UpdatePrices(&prices)
}

func Test_DefaultPriceCommand(t *testing.T) {
	input := CreateMessage("$100", "DefaultPriceCommand")
	result := command.ProcessCommand(input)
	expected := "100"
	if !strings.Contains(result, expected) {
		t.Errorf("Test case '%v' failed, expected: '%s', got: '%s'", input, expected+" to be in the response", result)
	}
}

func Test_RealDB(t *testing.T) {
	command.ProcessCommand(CreateMessage("$pur 100", "RealDB"))
	command.ProcessCommand(CreateMessage("$monAM 88", "RealDB"))
	command.ProcessCommand(CreateMessage("$monPM 124", "RealDB"))
	result := command.ProcessCommand(CreateMessage("$tuesAM 176", "RealDB"))
	expected := "RealDB's Tuesday morning price was set to: 176\n\n" +
		"You have a Large Spike! Wooo! Your best price will be on Tuesday evening!"
	if result != expected {
		t.Errorf("Test case failed, expected: '%s', got: '%s'", expected, result)
	}
}

func Test_Odds(t *testing.T) {
	command.ProcessCommand(CreateMessage("$pur 100", "RealDBOdds"))
	command.ProcessCommand(CreateMessage("$monAM 88", "RealDBOdds"))
	command.ProcessCommand(CreateMessage("$monPM 124", "RealDBOdds"))
	command.ProcessCommand(CreateMessage("$tuesAM 176", "RealDBOdds"))
	result := command.ProcessCommand(CreateMessage("$odds", "RealDBOdds"))
	expected := "You have a Large Spike! Wooo! Your best price will be on Tuesday evening!"
	expectedAfter := "You have a Large Spike! Sadly, your best price is behind you."
	if result != expected && result != expectedAfter {
		t.Errorf("Test case failed, expected: '%s', got: '%s'", expected, result)
	}
}

func Test_OddsMulti(t *testing.T) {
	result := SendCommands([]string{"pur 100", "monam 85", "monpm 124", "tueam 112", "odds"}, "MultiOdds")
	expected := "You have a Small Spike! Your best price will be on Wednesday morning!"
	expectedAfter := "You have a Small Spike! Sadly, your best price is behind you."
	if result != expected && result != expectedAfter {
		t.Errorf("Test case failed, expected: '%s', got: '%s'", expected, result)
	}
}

// Test that makes sure the am command keeps itself in the AM
func Test_CurrentAmTime_CommandDuringAm(t *testing.T) {
	input := database.TueAM
	result := command.CurrentAMTime(input)
	expected := database.TueAM
	if result != expected {
		t.Errorf("Test case '%v' failed, expected: '%v', got: '%v'", input, expected, result)
	}
}

func Test_CurrentAmTime_CommandDuringEarlyPm(t *testing.T) {
	input := database.TuePM
	result := command.CurrentAMTime(input)
	expected := database.TueAM
	if result != expected {
		t.Errorf("Test case '%v' failed, expected: '%v', got: '%v'", input, expected, result)
	}
}

func Test_CurrentAmTime_CommandDuringEarlyAm(t *testing.T) {
	input := database.MonPM
	result := command.CurrentAMTime(input)
	expected := database.MonAM
	if result != expected {
		t.Errorf("Test case '%v' failed, expected: '%v', got: '%v'", input, expected, result)
	}
}

func Test_BestPriceThisWeekEasy(t *testing.T) {
	time := time.Date(2020, time.January, 21, 18, 0, 0, 0, time.UTC)
	AddPrices([]uint16{100, 87, 98, 178, 400}, database.User{Name: "FirstSpike"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{105, 93, 88, 110, 187}, database.User{Name: "SecondSpike"}, database.GetSundayFromTime(time))
	result := command.BestPriceThisWeek(400, "FirstSpike", time)
	expected := 45.24
	if result != expected {
		t.Errorf("Test case failed, expected: '%f', got: '%f'", expected, result)
	}
}

func Test_BestPriceThisWeekLowMed(t *testing.T) {
	time := time.Date(2020, time.January, 14, 18, 0, 0, 0, time.UTC)
	AddPrices([]uint16{100, 87, 98, 178, 400}, database.User{Name: "FirstSpike"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{105, 93, 88, 110, 187}, database.User{Name: "SecondSpike"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{105, 93, 88, 110, 187}, database.User{Name: "ThirdSpike"}, database.GetSundayFromTime(time))
	result := command.BestPriceThisWeek(400, "FirstSpike", time)
	expected := 20.46
	if result != expected {
		t.Errorf("Test case failed, expected: '%f', got: '%f'", expected, result)
	}
}

func Test_BestPriceThisWeekHardMode(t *testing.T) {
	time := time.Date(2020, time.January, 8, 18, 0, 0, 0, time.UTC)
	AddPrices([]uint16{100, 87, 83, 79, 98, 178, 400}, database.User{Name: "FirstSpike"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{105, 93, 88, 84, 80, 76, 72}, database.User{Name: "SecondSpike"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{105, 93, 88, 84, 80, 76, 111}, database.User{Name: "ThirdSpike"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{105, 93, 88, 84, 111, 112, 156}, database.User{Name: "FourthSpike"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{105, 93, 111, 178, 600, 169, 124}, database.User{Name: "FifthSpike"}, database.GetSundayFromTime(time))
	result := command.BestPriceThisWeek(400, "FirstSpike", time)
	expected := 40.5
	if result != expected {
		t.Errorf("Test case failed, expected: '%f', got: '%f'", expected, result)
	}
}

func Test_BestPriceSmallSpikeWithADefiniteLarge(t *testing.T) {
	time := time.Date(2017, time.November, 7, 18, 0, 0, 0, time.UTC)
	AddPrices([]uint16{100, 124, 112, 178, 185}, database.User{Name: "FirstSpike"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{105, 93, 88, 110, 187}, database.User{Name: "SecondSpike"}, database.GetSundayFromTime(time))
	result := command.BestPriceThisWeek(185, "FirstSpike", time)
	expected := 0.00
	if result != expected {
		t.Errorf("Test case failed, expected: '%f', got: '%f'", expected, result)
	}
}

func Test_BestPriceSmallSpikeWithADefiniteSpike(t *testing.T) {
	time := time.Date(2017, time.November, 14, 18, 0, 0, 0, time.UTC)
	AddPrices([]uint16{100, 124, 112, 178, 185}, database.User{Name: "FirstSpike"}, database.GetSundayFromTime(time))
	AddPrices([]uint16{105, 93, 88, 85, 110}, database.User{Name: "SecondSpike"}, database.GetSundayFromTime(time))
	result := command.BestPriceThisWeek(185, "FirstSpike", time)
	expected := 7.13
	if result != expected {
		t.Errorf("Test case failed, expected: '%f', got: '%f'", expected, result)
	}
}
