package command_test

import (
	"sort"
	"testing"

	"gitlab.com/Texan/TomBookMe/command"
)

func Test_SpikeOdds_SortNames(t *testing.T) {
	spikes := []command.SpikeOdds{{Name: "Neil", Price: 100, Percent: 100, Time: 5}, {Name: "Alex", Price: 100, Percent: 100, Time: 5}}
	sort.Sort(command.Spikes(spikes))
	expected := []command.SpikeOdds{{Name: "Alex", Price: 100, Percent: 100, Time: 5}, {Name: "Neil", Price: 100, Percent: 100, Time: 5}}

	for i, spike := range spikes {
		if spike != expected[i] {
			t.Errorf("Test case failed, expected: '%v', got: '%v'", expected[i], spike)
		}
	}
}

func Test_SpikeOdds_SortPrices(t *testing.T) {
	spikes := []command.SpikeOdds{{Name: "Neil", Price: 200, Percent: 100, Time: 5}, {Name: "Alex", Price: 100, Percent: 100, Time: 5}}
	sort.Sort(command.Spikes(spikes))
	expected := []command.SpikeOdds{{Name: "Neil", Price: 200, Percent: 100, Time: 5}, {Name: "Alex", Price: 100, Percent: 100, Time: 5}}

	for i, spike := range spikes {
		if spike != expected[i] {
			t.Errorf("Test case failed, expected: '%v', got: '%v'", expected[i], spike)
		}
	}
}

func Test_SpikeOdds_SortPercent(t *testing.T) {
	spikes := []command.SpikeOdds{{Name: "Alex", Price: 200, Percent: 70, Time: 5}, {Name: "Neil", Price: 200, Percent: 100, Time: 5}}
	sort.Sort(command.Spikes(spikes))
	expected := []command.SpikeOdds{{Name: "Neil", Price: 200, Percent: 100, Time: 5}, {Name: "Alex", Price: 200, Percent: 70, Time: 5}}

	for i, spike := range spikes {
		if spike != expected[i] {
			t.Errorf("Test case failed, expected: '%v', got: '%v'", expected[i], spike)
		}
	}
}
