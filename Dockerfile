FROM golang:latest AS builder

WORKDIR /workdir
ENV SRC_DIR=/go/src/gitlab.com/texan/tombookme
ADD . $SRC_DIR

RUN cd $SRC_DIR; CGO_ENABLED=0 go build -o app; cp app /workdir/

FROM alpine:latest
COPY --from=builder /workdir/app /workdir/app
ENTRYPOINT ["./workdir/app"]