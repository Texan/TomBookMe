package creature

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

type Bugs struct {
	Bugs []Creature `json:"bugs"`
}

func GetBugsLeaving(month string) Bugs {
	path := "bugs.json"
	if os.Getenv("TOMBOOKME_PRODTOKEN") != "" {
		path = "creature/" + path
	}
	bugsJson, _ := os.Open(path)
	bugsBytes, _ := ioutil.ReadAll(bugsJson)
	bugsJson.Close()

	var bugs Bugs
	json.Unmarshal(bugsBytes, &bugs)

	for i := len(bugs.Bugs) - 1; i >= 0; i-- {
		if !(bugs.Bugs[i].LastMonth == month || bugs.Bugs[i].SecondaryLast == month) {
			bugs.Remove(i)
		}
	}

	return bugs
}

func GetBugsMessage(month string, bonusPuns bool, split string) string {
	message := ""
	bugsLeaving := GetBugsLeaving(month)
	start := 0
	length := len(bugsLeaving.Bugs)
	if split == "First" {
		length /= 2
	}
	if split == "Second" {
		start = length / 2
	}
	for i := start; i < length; i++ {
		message = message + "🐛 " + bugsLeaving.Bugs[i].Name + " (next seen: "
		if bugsLeaving.Bugs[i].LastMonth == month && bugsLeaving.Bugs[i].SecondaryFirst != "" {
			message = message + bugsLeaving.Bugs[i].SecondaryFirst[0:3] + ")\n\t"
		} else {
			message = message + bugsLeaving.Bugs[i].FirstMonth[0:3] + ")\n\t"
		}

		if bugsLeaving.Bugs[i].AppearTime != "" {
			message = message + "* " + bugsLeaving.Bugs[i].AppearTime + "-" + bugsLeaving.Bugs[i].DisappearTime

			if bugsLeaving.Bugs[i].SecondaryAppear != "" {
				message = message + " & " + bugsLeaving.Bugs[i].SecondaryAppear + "-" + bugsLeaving.Bugs[i].SecondaryDisappear
			}
		} else {
			message = message + "* All Day"
		}

		message = message + ": "

		if bonusPuns && bugsLeaving.Bugs[i].SecondaryDescription != "" {
			message = message + bugsLeaving.Bugs[i].SecondaryDescription
		} else {
			message = message + bugsLeaving.Bugs[i].Description
		}

		if length-1 != i {
			message = message + "\n"
		}
	}

	return message
}

func (f *Bugs) Remove(i int) {
	f.Bugs = append(f.Bugs[:i], f.Bugs[i+1:]...)
}
