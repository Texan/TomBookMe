package creature_test

import (
	"testing"

	"gitlab.com/Texan/TomBookMe/creature"
)

func Test_JulyFish(t *testing.T) {

	result := creature.GetFishLeaving("July")
	resultName := result.Fish[0].Name
	expected := "Tadpole"
	if resultName != expected {
		t.Errorf("Test case failed, expected: '%s', got: '%s'", expected, result)
	}
}

func Test_TwoJuneFish(t *testing.T) {

	result := creature.GetFishLeaving("June")
	resultLen := len(result.Fish)
	expected := 2
	if resultLen != expected {
		t.Errorf("Test case failed, expected: '%d', got: '%d'", expected, resultLen)
	}
}

func Test_JuneMessage(t *testing.T) {

	result := creature.GetFishMessage("June", false, "")
	expected := "🐠 Char (next seen: Sep)\n\t* 4p-9a: Finding a char should be as easy as a, b, c.\n🐠 Cherry Salmon (next seen: Sep)\n\t* 4p-9a: Catching one of these would be the cherry on top of this month."
	if result != expected {
		t.Errorf("Test case failed, expected: '%s', got: '%s'", expected, result)
	}
}

func Test_JuneMessageSplitFirst(t *testing.T) {

	result := creature.GetFishMessage("June", false, "First")
	expected := "🐠 Char (next seen: Sep)\n\t* 4p-9a: Finding a char should be as easy as a, b, c."
	if result != expected {
		t.Errorf("Test case failed, expected: '%s', got: '%s'", expected, result)
	}
}

func Test_JuneMessageSplitSecond(t *testing.T) {

	result := creature.GetFishMessage("June", false, "Second")
	expected := "🐠 Cherry Salmon (next seen: Sep)\n\t* 4p-9a: Catching one of these would be the cherry on top of this month."
	if result != expected {
		t.Errorf("Test case failed, expected: '%s', got: '%s'", expected, result)
	}
}

func Test_FiveAugustFish(t *testing.T) {

	result := creature.GetFishLeaving("August")
	resultLen := len(result.Fish)
	expected := 5
	if resultLen != expected {
		t.Errorf("Test case failed, expected: '%d', got: '%d'", expected, resultLen)
	}
}

func Test_WrongMonthFish(t *testing.T) {

	result := creature.GetFishLeaving("Wrong")
	resultLen := len(result.Fish)
	expected := 0
	if resultLen != expected {
		t.Errorf("Test case failed, expected: '%d', got: '%d'", expected, resultLen)
	}
}

func Test_WrongMonthMessage(t *testing.T) {

	result := creature.GetFishMessage("Wrong", false, "")
	expected := ""
	if result != expected {
		t.Errorf("Test case failed, expected: '%s', got: '%s'", expected, result)
	}
}
