package creature

type Creature struct {
	Name                 string `json:"name"`
	FirstMonth           string `json:"first_month"`
	LastMonth            string `json:"last_month"`
	SecondaryFirst       string `json:"secondary_first"`
	SecondaryLast        string `json:"secondary_last"`
	AppearTime           string `json:"appear_time"`
	DisappearTime        string `json:"disappear_time"`
	SecondaryAppear      string `json:"secondary_appear"`
	SecondaryDisappear   string `json:"secondary_disappear"`
	Location             string `json:"location"`
	Description          string `json:"description"`
	SecondaryDescription string `json:"secondary_description"`
}
