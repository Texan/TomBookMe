package creature_test

import (
	"testing"

	"gitlab.com/Texan/TomBookMe/creature"
)

func Test_JulyDiving(t *testing.T) {

	result := creature.GetDivingLeaving("July")
	resultName := result.Diving[0].Name
	expected := "Seaweed"
	if resultName != expected {
		t.Errorf("Test case failed, expected: '%s', got: '%s'", expected, result)
	}
}

func Test_TwoAugustDiving(t *testing.T) {

	result := creature.GetDivingLeaving("August")
	resultLen := len(result.Diving)
	expected := 2
	if resultLen != expected {
		t.Errorf("Test case failed, expected: '%d', got: '%d'", expected, resultLen)
	}
}

func Test_WrongMonthDiving(t *testing.T) {

	result := creature.GetDivingLeaving("Wrong")
	resultLen := len(result.Diving)
	expected := 0
	if resultLen != expected {
		t.Errorf("Test case failed, expected: '%d', got: '%d'", expected, resultLen)
	}
}
