package creature

import (
	"time"
)

func GetCreaturesLeaving(current time.Time) []string {
	messages := []string{}
	message := "Friendly Reminder: "
	bonusPuns := false
	onlyIfSplit := false
	if current.AddDate(0, 0, 1).Month() != current.Month() {
		message = message + "Today is the last day to catch the following:\n"
		bonusPuns = true
	} else if current.AddDate(0, 0, 2).Month() != current.Month() {
		message = message + "Tomorrow is the last day to catch the following:\n"
	} else if current.AddDate(0, 0, 8).Month() != current.Month() && current.AddDate(0, 0, 8).Day() == 1 {
		onlyIfSplit = true
		message = message + "This is the last week to catch the following:\n"
	} else {
		return nil
	}

	bugs := GetBugsMessage(current.Month().String(), bonusPuns, "")
	if len(message)+len(bugs) > 1000 {
		message = message + GetBugsMessage(current.Month().String(), bonusPuns, "First")
		messages = append(messages, message)
		message = GetBugsMessage(current.Month().String(), bonusPuns, "Second")
		messages = append(messages, message)
		message = ""
	} else {
		message = message + bugs + "\n"
	}

	fish := GetFishMessage(current.Month().String(), bonusPuns, "")
	if len(message)+len(fish) > 1000 {
		messages = append(messages, message)
		if len(fish) > 1000 {
			message = GetFishMessage(current.Month().String(), bonusPuns, "First")
			messages = append(messages, message)
			message = GetFishMessage(current.Month().String(), bonusPuns, "Second")
			messages = append(messages, message)
			message = ""
		} else {
			message = fish + "\n"
		}
	} else {
		message = message + fish + "\n"
	}

	diving := GetDivingMessage(current.Month().String(), bonusPuns, "")
	if len(message)+len(diving) > 1000 {
		messages = append(messages, message)
		if len(diving) > 1000 {
			message = GetDivingMessage(current.Month().String(), bonusPuns, "First")
			messages = append(messages, message)
			message = GetDivingMessage(current.Month().String(), bonusPuns, "Second")
			messages = append(messages, message)
			message = ""
		} else {
			message = diving + "\n"
		}
	} else {
		message = message + diving + "\n"
	}

	messages = append(messages, message)

	if onlyIfSplit && len(messages) == 1 {
		return nil
	}

	return messages
}
