package creature

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

type Diving struct {
	Diving []Creature `json:"diving"`
}

func GetDivingLeaving(month string) Diving {
	path := "diving.json"
	if os.Getenv("TOMBOOKME_PRODTOKEN") != "" {
		path = "creature/" + path
	}
	divingJson, _ := os.Open(path)
	divingBytes, _ := ioutil.ReadAll(divingJson)
	divingJson.Close()

	var diving Diving
	json.Unmarshal(divingBytes, &diving)

	for i := len(diving.Diving) - 1; i >= 0; i-- {
		if !(diving.Diving[i].LastMonth == month || diving.Diving[i].SecondaryLast == month) {
			diving.Remove(i)
		}
	}

	return diving
}

func GetDivingMessage(month string, bonusPuns bool, split string) string {
	message := ""
	divingLeaving := GetDivingLeaving(month)
	start := 0
	length := len(divingLeaving.Diving)
	if split == "First" {
		length /= 2
	}
	if split == "Second" {
		start = length / 2
	}
	for i := start; i < length; i++ {
		message = message + "🐙 " + divingLeaving.Diving[i].Name + " (next seen: "
		if divingLeaving.Diving[i].LastMonth == month && divingLeaving.Diving[i].SecondaryFirst != "" {
			message = message + divingLeaving.Diving[i].SecondaryFirst[0:3] + ")\n\t"
		} else {
			message = message + divingLeaving.Diving[i].FirstMonth[0:3] + ")\n\t"
		}

		if divingLeaving.Diving[i].AppearTime != "" {
			message = message + "* " + divingLeaving.Diving[i].AppearTime + "-" + divingLeaving.Diving[i].DisappearTime

			if divingLeaving.Diving[i].SecondaryAppear != "" {
				message = message + " & " + divingLeaving.Diving[i].SecondaryAppear + "-" + divingLeaving.Diving[i].SecondaryDisappear
			}
		} else {
			message = message + "* All Day"
		}

		message = message + ": "

		if bonusPuns && divingLeaving.Diving[i].SecondaryDescription != "" {
			message = message + divingLeaving.Diving[i].SecondaryDescription
		} else {
			message = message + divingLeaving.Diving[i].Description
		}

		if length-1 != i {
			message = message + "\n"
		}
	}

	return message
}

func (f *Diving) Remove(i int) {
	f.Diving = append(f.Diving[:i], f.Diving[i+1:]...)
}
