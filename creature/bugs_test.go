package creature_test

import (
	"testing"

	"gitlab.com/Texan/TomBookMe/creature"
)

func Test_JulyBugs(t *testing.T) {

	result := creature.GetBugsLeaving("July")
	resultName := result.Bugs[0].Name
	expected := "Honeybee"
	if resultName != expected {
		t.Errorf("Test case failed, expected: '%s', got: '%s'", expected, result)
	}
}

func Test_EightJuneBugs(t *testing.T) {

	result := creature.GetBugsLeaving("June")
	resultLen := len(result.Bugs)
	expected := 8
	if resultLen != expected {
		t.Errorf("Test case failed, expected: '%d', got: '%d'", expected, resultLen)
	}
}

func Test_TwentyOneAugustBugs(t *testing.T) {

	result := creature.GetBugsLeaving("August")
	resultLen := len(result.Bugs)
	expected := 21
	if resultLen != expected {
		t.Errorf("Test case failed, expected: '%d', got: '%d'", expected, resultLen)
	}
}

func Test_WrongMonthBugs(t *testing.T) {

	result := creature.GetBugsLeaving("Wrong")
	resultLen := len(result.Bugs)
	expected := 0
	if resultLen != expected {
		t.Errorf("Test case failed, expected: '%d', got: '%d'", expected, resultLen)
	}
}
