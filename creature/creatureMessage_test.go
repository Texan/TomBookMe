package creature_test

import (
	"testing"
	"time"

	"gitlab.com/Texan/TomBookMe/creature"
)

func Test_AugustCreatureOneWeekLeft(t *testing.T) {
	testTime := time.Date(2020, 8, 24, 16, 0, 0, 0, time.UTC)
	result := creature.GetCreaturesLeaving(testTime)
	resultLen := len(result)
	expected := 3
	if resultLen != expected {
		t.Errorf("Test case failed, expected: '%d', got: '%d'", expected, resultLen)
	}
}

func Test_AugustCreatureOneDayAfterOneWeekLeft(t *testing.T) {
	testTime := time.Date(2020, 8, 25, 16, 0, 0, 0, time.UTC)
	result := creature.GetCreaturesLeaving(testTime)
	resultLen := len(result)
	expected := 0
	if resultLen != expected {
		t.Errorf("Test case failed, expected: '%d', got: '%d'", expected, resultLen)
	}
}

func Test_AugustCreatureLastDay(t *testing.T) {
	testTime := time.Date(2020, 8, 31, 16, 0, 0, 0, time.UTC)
	result := creature.GetCreaturesLeaving(testTime)
	resultLen := len(result)
	expected := 3
	if resultLen != expected {
		t.Errorf("Test case failed, expected: '%d', got: '%d'", expected, resultLen)
	}
}
