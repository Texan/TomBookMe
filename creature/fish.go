package creature

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

const fishEmoji = "🐠"

type Fish struct {
	Fish []Creature `json:"fish"`
}

func GetFishLeaving(month string) Fish {
	path := "fish.json"
	if os.Getenv("TOMBOOKME_PRODTOKEN") != "" {
		path = "creature/" + path
	}
	fishJson, _ := os.Open(path)
	fishBytes, _ := ioutil.ReadAll(fishJson)
	fishJson.Close()

	var fish Fish
	json.Unmarshal(fishBytes, &fish)

	for i := len(fish.Fish) - 1; i >= 0; i-- {
		if !(fish.Fish[i].LastMonth == month || fish.Fish[i].SecondaryLast == month) {
			fish.Remove(i)
		}
	}

	return fish
}

func GetFishMessage(month string, bonusPuns bool, split string) string {
	message := ""
	fishLeaving := GetFishLeaving(month)
	start := 0
	length := len(fishLeaving.Fish)
	if split == "First" {
		length /= 2
	}
	if split == "Second" {
		start = length / 2
	}
	for i := start; i < length; i++ {
		message = message + "🐠 " + fishLeaving.Fish[i].Name + " (next seen: "
		if fishLeaving.Fish[i].LastMonth == month && fishLeaving.Fish[i].SecondaryFirst != "" {
			message = message + fishLeaving.Fish[i].SecondaryFirst[0:3] + ")\n\t"
		} else {
			message = message + fishLeaving.Fish[i].FirstMonth[0:3] + ")\n\t"
		}

		if fishLeaving.Fish[i].AppearTime != "" {
			message = message + "* " + fishLeaving.Fish[i].AppearTime + "-" + fishLeaving.Fish[i].DisappearTime

			if fishLeaving.Fish[i].SecondaryAppear != "" {
				message = message + " & " + fishLeaving.Fish[i].SecondaryAppear + "-" + fishLeaving.Fish[i].SecondaryDisappear
			}
		} else {
			message = message + "* All Day"
		}

		message = message + ": "

		if bonusPuns && fishLeaving.Fish[i].SecondaryDescription != "" {
			message = message + fishLeaving.Fish[i].SecondaryDescription
		} else {
			message = message + fishLeaving.Fish[i].Description
		}

		if length-1 != i {
			message = message + "\n"
		}
	}

	return message
}

func (f *Fish) Remove(i int) {
	f.Fish = append(f.Fish[:i], f.Fish[i+1:]...)
}
