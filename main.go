package main

import (
	"encoding/json"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"github.com/spf13/viper"
	"gitlab.com/Texan/TomBookMe/command"
	"gitlab.com/Texan/TomBookMe/creature"
	"gitlab.com/Texan/TomBookMe/database"
	"gitlab.com/Texan/TomBookMe/message"
	"gitlab.com/Texan/TomBookMe/turnip"
)

func main() {
	database.Connect(viper.GetString("dbpath"))
	mh := message.MessageHandler(os.Getenv("TOMBOOKME_PRODTOKEN"))

	latestVersion := command.GetVersion(false)
	latestRelease := database.GetReleaseNotes()
	if latestRelease != latestVersion && latestRelease != "Error" {
		mh.SendMessage(command.GetVersion(true))
		database.UpdateReleaseNotes(latestVersion)
	}

	ticker := time.NewTicker(time.Minute)
	quit := make(chan struct{})
	go func() {
		for {
			select {
			case <-ticker.C:
				bestCoastTime, _ := time.LoadLocation("America/Vancouver")
				now := time.Now().In(bestCoastTime)

				if now.Weekday() == 0 && now.Hour() == 4 && now.Minute() == 0 {
					mh.SendMessage("Good Morning Stalkholders!\n" +
						"Please remember to input your prices from Daisy Mae using the $price command! (Ex: $100)")
				}
				if now.Weekday() != 0 && now.Weekday() != 6 &&
					(now.Hour() == 8 || now.Hour() == 17) && now.Minute() == 30 {
					//timeOfDay := "Morning's"
					//if now.Hour() == 17 {
					//timeOfDay = "Evening's"
					//}
					//mh.SendMessage(now.Weekday().String() + " " + timeOfDay + " Spike Update\n\n" + command.SpikeView(now, true))
				}
				if now.Weekday() != 0 && now.Minute() == 45 {
					thisWeek := database.GetWeeksPrices(database.GetSundayFromTime(now))
					for _, p := range thisWeek {
						priceTime := database.GetPriceTimeFromCommand(p.BestPrice())
						if priceTime.Message() != "Unknown Unknown" {
							user := database.GetUser(p.User)
							userTime, _ := time.LoadLocation(user.TimeZone)
							userNow := time.Now().In(userTime)
							if userNow.Weekday().String() == priceTime.Weekday() {
								if userNow.Hour() == 7 && priceTime.TimeOfDay() == "morning" ||
									userNow.Hour() == 11 && priceTime.TimeOfDay() == "evening" {
									if p.Pattern == "LargeSpike" {
										odds := turnip.CalculateOdds(&p)
										mh.SendMessage("Upcoming Large Spike in " + user.Name + "'s town! All aboard the turnip train!\nFriend Code: " + user.SwitchCode + "\nExpected Price: " + strconv.FormatUint(uint64(odds.AveragePrice), 10))
									} else if p.Pattern == "SmallSpike" {
										odds := turnip.CalculateOdds(&p)
										mh.SendMessage("Upcoming Small Spike in " + user.Name + "'s town!\nFriend Code: " + user.SwitchCode + "\nExpected Price: " + strconv.FormatUint(uint64(odds.AveragePrice), 10))
									}
								}
							}
						}
					}
				}
				if now.Hour() == 7 && now.Minute() == 0 {
					leaving := creature.GetCreaturesLeaving(now)
					if leaving != nil {
						for _, message := range leaving {
							mh.SendMessage(message)
						}
					}
				}
			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()

	r := mux.NewRouter()
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		var m message.Message

		err := json.NewDecoder(r.Body).Decode(&m)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		if m.Text[0] == '$' {
			mh.SendMessage(command.ProcessCommand((m)))
		}
	})
	port := os.Getenv("TOMBOOKME_PORT")
	if port == "" {
		port = ":8080"
	}
	http.ListenAndServe(port, r)
}
