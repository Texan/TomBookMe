# 2020.3.2 (May 26, 2020)
* Added: A reminder on the last two days of the month about the outgoing bugs and fish
* Enhancement: Spike view is better formatted for ongoing and unknown spikes
* Enhancement: Small spike odds will display in the spike view when the odds of a new large spike are below 80%
* Enhancement: Small spikes will now have best prices calculated for them
* Fixed: Guaranteed small spikes still decreasing on Thursday morning will now list Saturday morning as the spike time
* Fixed: Small spikes are sorted correctly when there are unknown spike times
* Fixed: Spikes are sorted after rounding instead of before
* Fixed: Best price odds format to two decimal places
* Fixed: Best price accounts for small and large spikes
* Fixed: Best price chances are now correctly capped between 0% and 100%
* Fixed: Unknown spike times will now state that more elegantly
* Fixed: Spikes past peak will mention they've already peaked, much like you

# 2020.3.1 (May 24, 2020)
* Enhancement: Spike odds display two decimal places when over 99%
* Enhancement: Small Spike odds lowered for certain percentage brackets 
* Fixed: Shortened names are now displayed everywhere
* Fixed: Help command is more aligned

# 2020.3 (May 20, 2020)
* Added: Spike Prediction Models
* Added: Large Spike odds view using the $pog command
* Added: This really neat metric that predicts if a large spike will be the best price we see this week
* Added: Morning and evening updates on our spike outlook
* Added: Spike announcements fifteen minutes before a spike
* Enhancement: Guaranteed spikes with have their peak displayed
* Enhancement: Better messages for 100% odds
* Enhancement: Names are used in place of your in many places
* Fixed: Nothing; this is feature creep at it's finest
* Refactored: Over a thousand lines because time zones

# 2020.2.1 (May 18, 2020)
* Added: Prices view using the $prices command
* Enhancement: Help command now lists view commands
* Fixed: AM command now properly sets the AM price during the AM hours
* Fixed: Average price and price range guess at a purchase price if it was not set

# 2020.2 (May 17, 2020)
* Added: Users can track turnip prices in multiple towns using the -u switch (for more info, check out the $help command)
* Added: Average price and price ranges are now displayed with odds
* Added: Release notes are sent out for new releases (Hi!)
* Enhancement: Odds are now sorted
* Enhancement: Odds are rounded to a whole number
* Fixed: Turnip reminder is now only sent out on Sunday morning

# 2020.1 (May 16, 2020)
* Initial Release
* Added: Price tracking
* Added: Timezone support
* Added: Switch code formatting