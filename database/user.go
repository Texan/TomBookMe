package database

type User struct {
	ID         uint   `gorm:"primary_key"`
	Name       string `gorm:"name"`
	SwitchCode string `gorm:"switch_code"`
	TimeZone   string `gorm:"time_zone"`
	UserID     string `gorm:"user_id"`
}
