package database_test

import (
	"testing"

	"gitlab.com/Texan/TomBookMe/database"
)

func Test_LargeSpikeBestPrices(t *testing.T) {
	tests := []database.Prices{
		{Pattern: "LargeSpike", Purchase: 100, MonAM: 87, MonPM: 98, TueAM: 176},
		{Pattern: "LargeSpike", Purchase: 100, MonAM: 87, MonPM: 83, TueAM: 98, TuePM: 176},
		{Pattern: "LargeSpike", Purchase: 100, MonAM: 87, MonPM: 98, TueAM: 176, TuePM: 405},
		{Pattern: "LargeSpike", Purchase: 100, MonAM: 87, MonPM: 98, TueAM: 176, TuePM: 405, WedAM: 169},
	}

	expected := []string{"TuePM", "WedAM", "TuePM", "TuePM"}

	for i, spike := range tests {
		result := spike.BestPrice()
		if result != expected[i] {
			t.Errorf("Test case '%v' failed, expected: '%v', got: '%v'", spike, expected[i], result)
		}
	}
}

func Test_SmallSpikeBestPrices(t *testing.T) {
	tests := []database.Prices{
		{Pattern: "SmallSpike", Purchase: 100, MonAM: 59},
		{Pattern: "SmallSpike", Purchase: 100, MonAM: 90, MonPM: 86, TueAM: 98, TuePM: 132},
		{Pattern: "SmallSpike", Purchase: 100, MonAM: 90, MonPM: 98, TueAM: 176, TuePM: 187},
		{Pattern: "SmallSpike", Purchase: 100, MonAM: 87, MonPM: 83, TueAM: 78, TuePM: 75, WedAM: 123},
		{Pattern: "SmallSpike", Purchase: 100, MonAM: 87, MonPM: 83, TueAM: 78, TuePM: 75, WedAM: 71, WedPM: 67, ThuAM: 63},
	}

	expected := []string{"Unknown", "WedPM", "TuePM", "ThuPM", "SatAM"}

	for i, spike := range tests {
		result := spike.BestPrice()
		if result != expected[i] {
			t.Errorf("Test case '%v' failed, expected: '%v', got: '%v'", spike, expected[i], result)
		}
	}
}
