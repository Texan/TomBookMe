package database

import (
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/mattn/go-sqlite3"
)

var DB *gorm.DB

func Connect(path string) {
	DB, _ = gorm.Open("sqlite3", path)
	DB.AutoMigrate(&Prices{})
	DB.AutoMigrate(&User{})
}

func AddPrices(user User, week string) error {
	if user.ID == 0 {
		return nil
	}
	prices := Prices{
		User:    user.UserID,
		Week:    week,
		Pattern: "Unknown",
	}
	DB.Create(&prices)
	return nil
}

func GetPrices(user User, week string) (prices Prices) {
	if user.ID == 0 {
		return prices
	}
	DB.Where("user = ?", user.UserID).Where("week = ?", week).Find(&prices)
	if prices.ID == 0 {
		AddPrices(user, week)
		DB.Where("user = ?", user.UserID).Where("week = ?", week).Find(&prices)
	}
	return prices
}

func UpdatePrices(prices *Prices) {
	if prices.ID != 0 {
		DB.Save(&prices)
	}
}

func GetWeeksPrices(week string) (prices []Prices) {
	DB.Where("week = ?", week).Find(&prices)
	return prices
}

func AddUser(name string, userID string) error {
	if userID == "" {
		return nil
	}
	user := User{
		Name:     name,
		TimeZone: "America/Chicago",
		UserID:   userID,
	}
	DB.Create(&user)
	return nil
}

func GetUser(id string) (user User) {
	if id == "" {
		return User{}
	}

	DB.Where("user_id = ?", id).Find(&user)
	return user
}

func GetName(id string) string {
	user := GetUser(id)
	return user.Name
}

func AddUserSwitchCode(user User, switchCode string) {
	user.SwitchCode = switchCode
	DB.Save(&user)
}

func AddUserTimeZone(user User, timeZone string) {
	user.TimeZone = timeZone
	DB.Save(&user)
}

func GetSunday(user User, thisWeek bool) string {
	nsa, _ := time.LoadLocation(user.TimeZone)
	now := time.Now().In(nsa)
	weekday := int(now.Weekday()) * -1
	lastWeek := -7
	if thisWeek {
		lastWeek = 0
	}
	now = now.AddDate(0, 0, weekday+lastWeek)
	return now.Format("2006-01-02")
}

func GetSundayFromTime(time time.Time) string {
	now := time
	weekday := int(time.Weekday()) * -1
	now = now.AddDate(0, 0, weekday)
	return now.Format("2006-01-02")
}

func GetLastWeeksPattern(user User) string {
	if user.ID == 0 {
		return "Unknown"
	}

	var prices Prices
	week := GetSunday(user, false)
	DB.Where("user = ?", user.UserID).Where("week = ?", week).Find(&prices)
	if prices.ID == 0 {
		return "Unknown"
	}
	return prices.Pattern
}

func GetReleaseNotes() string {
	var user User
	DB.Where("name = ?", "TomBookMe").Find(&user)
	if user.ID == 0 {
		return "Error"
	}
	return user.SwitchCode
}

func UpdateReleaseNotes(version string) {
	var user User
	DB.Where("name = ?", "TomBookMe").Find(&user)
	if user.ID == 0 {
		return
	}
	user.SwitchCode = version
	DB.Save(&user)
}
