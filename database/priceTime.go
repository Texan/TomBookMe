package database

import (
	"strings"
	"time"
)

type PriceTime int

const (
	Purchase PriceTime = iota
	MonAM
	MonPM
	TueAM
	TuePM
	WedAM
	WedPM
	ThuAM
	ThuPM
	FriAM
	FriPM
	SatAM
	SatPM
)

func (time PriceTime) String() string {
	times := []string{
		"Purchase", "MonAM", "MonPM", "TueAM", "TuePM", "WedAM", "WedPM", "ThuAM", "ThuPM", "FriAM", "FriPM", "SatAM", "SatPM",
	}

	if time < Purchase || time > SatPM {
		return "Unknown"
	}

	return times[time]
}

func (time PriceTime) Message() string {
	if time == Purchase {
		return "purchase"
	}

	return time.Weekday() + " " + time.TimeOfDay()
}

func (time PriceTime) Weekday() string {
	times := []string{
		"Sunday", "Monday", "Monday", "Tuesday", "Tuesday", "Wednesday", "Wednesday", "Thursday", "Thursday", "Friday", "Friday", "Saturday", "Saturday",
	}

	if time < Purchase || time > SatPM {
		return "Unknown"
	}

	return times[time]
}

func (time PriceTime) TimeOfDay() string {
	if time < Purchase || time > SatPM {
		return "Unknown"
	} else if time == Purchase || time%2 == 1 {
		return "morning"
	} else {
		return "evening"
	}
}

func GetPriceTimeFromCommand(time string) PriceTime {
	switch strings.ToLower(time) {
	case "pur", "purchase", "sun", "sunam", "sunpm", "daisy", "joan", "bought", "turnips":
		return Purchase
	case "monam":
		return MonAM
	case "monpm":
		return MonPM
	case "tueam", "tuesam":
		return TueAM
	case "tuepm", "tuespm":
		return TuePM
	case "wedam", "wedsam":
		return WedAM
	case "wedpm", "wedspm":
		return WedPM
	case "tham", "thuam", "thuram", "thursam":
		return ThuAM
	case "thpm", "thupm", "thurpm", "thurspm":
		return ThuPM
	case "friam":
		return FriAM
	case "fripm":
		return FriPM
	case "satam":
		return SatAM
	case "satpm":
		return SatPM
	default:
		return -1
	}
}

func GetPriceTimeFromTime(time time.Time) PriceTime {
	now := time
	if now.Weekday() == 0 {
		return Purchase
	}

	offset := 0
	if now.Hour() < 6 {
		offset = -1
	} else if now.Hour() >= 12 {
		offset = 1
	}

	return PriceTime(int(now.Weekday())*2 - 1 + offset)
}
