package database

import (
	"math"
	"strconv"
)

type Prices struct {
	ID       uint   `gorm:"primary_key"`
	User     string `gorm:"user"`
	Week     string `gorm:"week"`
	Pattern  string `gorm:"pattern"`
	Purchase uint16 `gorm:"purchase"`
	MonAM    uint16 `gorm:"mon_am"`
	MonPM    uint16 `gorm:"mon_pm"`
	TueAM    uint16 `gorm:"tue_am"`
	TuePM    uint16 `gorm:"tue_pm"`
	WedAM    uint16 `gorm:"wed_am"`
	WedPM    uint16 `gorm:"wed_pm"`
	ThuAM    uint16 `gorm:"thu_am"`
	ThuPM    uint16 `gorm:"thu_pm"`
	FriAM    uint16 `gorm:"fri_am"`
	FriPM    uint16 `gorm:"fri_pm"`
	SatAM    uint16 `gorm:"sat_am"`
	SatPM    uint16 `gorm:"sat_pm"`
}

func (prices Prices) ToString() string {
	output := GetName(prices.User) + "'s Current Prices:\n"
	output += "Purchase: " + strconv.FormatFloat(float64(prices.Purchase), 'f', 0, 64)
	if prices.MonAM != 0 || prices.MonPM != 0 {
		output += "\nMonday: " + strconv.FormatFloat(float64(prices.MonAM), 'f', 0, 64) + " - " + strconv.FormatFloat(float64(prices.MonPM), 'f', 0, 64)
	}
	if prices.TueAM != 0 || prices.TuePM != 0 {
		output += "\nTuesday: " + strconv.FormatFloat(float64(prices.TueAM), 'f', 0, 64) + " - " + strconv.FormatFloat(float64(prices.TuePM), 'f', 0, 64)
	}
	if prices.WedAM != 0 || prices.WedPM != 0 {
		output += "\nWednesday: " + strconv.FormatFloat(float64(prices.WedAM), 'f', 0, 64) + " - " + strconv.FormatFloat(float64(prices.WedPM), 'f', 0, 64)
	}
	if prices.ThuAM != 0 || prices.ThuPM != 0 {
		output += "\nThursday: " + strconv.FormatFloat(float64(prices.ThuAM), 'f', 0, 64) + " - " + strconv.FormatFloat(float64(prices.ThuPM), 'f', 0, 64)
	}
	if prices.FriAM != 0 || prices.FriPM != 0 {
		output += "\nFriday: " + strconv.FormatFloat(float64(prices.FriAM), 'f', 0, 64) + " - " + strconv.FormatFloat(float64(prices.FriPM), 'f', 0, 64)
	}
	if prices.SatAM != 0 || prices.SatPM != 0 {
		output += "\nSaturday: " + strconv.FormatFloat(float64(prices.SatAM), 'f', 0, 64) + " - " + strconv.FormatFloat(float64(prices.SatPM), 'f', 0, 64)
	}
	return output
}

func (prices Prices) BestPrice() string {
	switch prices.Pattern {
	case "LargeSpike":
		spikePrices := []uint16{prices.MonPM, prices.TueAM, prices.TuePM, prices.WedAM, prices.WedPM, prices.ThuAM, prices.ThuPM}
		spikeTime := []string{"TuePM", "WedAM", "WedPM", "ThuAM", "ThuPM", "FriAM", "FriPM"}
		for i, spikePrice := range spikePrices {
			if spikePrice >= uint16(math.Ceil(0.9*float64(prices.Purchase))) &&
				spikePrice <= uint16(math.Ceil(1.4*float64(prices.Purchase))) {
				return spikeTime[i]
			}
		}
	case "SmallSpike":
		spikePrices := []uint16{prices.MonPM, prices.TueAM, prices.TuePM, prices.WedAM, prices.WedPM, prices.ThuAM, prices.ThuPM}
		spikeTime := []string{"WedAM", "WedPM", "ThuAM", "ThuPM", "FriAM", "FriPM", "SatAM"}
		for i, spikePrice := range spikePrices {
			if spikePrice >= uint16(math.Ceil(0.9*float64(prices.Purchase))) &&
				spikePrice <= uint16(math.Ceil(1.4*float64(prices.Purchase))) {
				if i == 0 && prices.MonAM >= uint16(math.Ceil(0.9*float64(prices.Purchase))) &&
					prices.MonAM <= uint16(math.Ceil(1.4*float64(prices.Purchase))) {
					return "TuePM"
				}
				return spikeTime[i]
			} else if i == 5 && spikePrice != 0 {
				return spikeTime[6]
			}
		}
	}

	return "Unknown"
}

func (prices Prices) GetPrice(time PriceTime) uint16 {
	switch time {
	case Purchase:
		return prices.Purchase
	case MonAM:
		return prices.MonAM
	case MonPM:
		return prices.MonPM
	case TueAM:
		return prices.TueAM
	case TuePM:
		return prices.TuePM
	case WedAM:
		return prices.WedAM
	case WedPM:
		return prices.WedPM
	case ThuAM:
		return prices.ThuAM
	case ThuPM:
		return prices.ThuPM
	case FriAM:
		return prices.FriAM
	case FriPM:
		return prices.FriPM
	case SatAM:
		return prices.SatAM
	case SatPM:
		return prices.SatPM
	default:
		return 0
	}
}

func (prices *Prices) SetPrice(time PriceTime, price uint16) {
	switch time {
	case Purchase:
		prices.Purchase = price
	case MonAM:
		prices.MonAM = price
	case MonPM:
		prices.MonPM = price
	case TueAM:
		prices.TueAM = price
	case TuePM:
		prices.TuePM = price
	case WedAM:
		prices.WedAM = price
	case WedPM:
		prices.WedPM = price
	case ThuAM:
		prices.ThuAM = price
	case ThuPM:
		prices.ThuPM = price
	case FriAM:
		prices.FriAM = price
	case FriPM:
		prices.FriPM = price
	case SatAM:
		prices.SatAM = price
	case SatPM:
		prices.SatPM = price
	}
}
