git clone "git@gitlab.com:Texan/TomBookMe.git" "TomBookMe/" 2> /dev/null || (cd "TomBookMe/" ; git pull)
echo "Switching directory..."
cd TomBookMe
echo "Building..."
go build
echo "Clearing other instances..."
systemctl stop TomBookMe
echo "Running..."
systemctl start TomBookMe